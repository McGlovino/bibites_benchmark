﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.UIScipts;

namespace Assets.Scripts.SettingScripts {
    
    public class GameSettingsHandle : MonoBehaviour {
        [Header("Panels")]
        public GameObject[] Panels;
        public GameObject LoadingPanel;


        private SettingGroup SimulationOptions = new SettingGroup {
            Settings = new List<SimulationSetting> {
                GameSettings.Instance.pelletCollision,
                GameSettings.Instance.pelletRotation,
                GameSettings.Instance.voidAvoidance,
                GameSettings.Instance.voidAvoidanceDistance

            }
        };

        private SettingGroup WorldParameters = new SettingGroup {
            Settings = new List<SimulationSetting> {
                GameSettings.Instance.SimulationSize,
                GameSettings.Instance.biomassDensity,
                GameSettings.Instance.pelletGrowth,
                GameSettings.Instance.pelletSize,
                GameSettings.Instance.meatRot,
                GameSettings.Instance.meatRotTime,
                GameSettings.Instance.bibiteGrowth,
                GameSettings.Instance.capBibiteGrowth,
                GameSettings.Instance.capNumber,

            }
        };

        private SettingGroup PhysicsParmeters = new SettingGroup {
            Settings = new List<SimulationSetting> {
                GameSettings.Instance.dragCoefficient,
                GameSettings.Instance.forwardForce,
                GameSettings.Instance.backwardFraction,
                GameSettings.Instance.defaultBibiteMass,
                GameSettings.Instance.defaultPelletMass,
            }
        };

        private SettingGroup  BibiteConstants = new SettingGroup {
            Settings = new List<SimulationSetting> {
                GameSettings.Instance.maxEnergyToMaxHealthRatio,
                GameSettings.Instance.maxDietEfficiency,
                GameSettings.Instance.minDietEfficiency,
                GameSettings.Instance.metabolismCost,
                GameSettings.Instance.moveCost,
                GameSettings.Instance.neuronBirthCost,
                GameSettings.Instance.synapseBirthCost,
                GameSettings.Instance.pheromoneProductionCost
            }
        };
        
        private SettingGroup MeatBalance = new SettingGroup {
            Settings = new List<SimulationSetting> {
                GameSettings.Instance.baseBodyEnergyRatio,
                GameSettings.Instance.healthBodyRatio,
                GameSettings.Instance.viewAngleBodyCost,
                GameSettings.Instance.viewRadiusBodyCost,
                GameSettings.Instance.strengthBodyCost,
            }
        };

        private SettingGroup VisualGenes = new SettingGroup { 
            Settings = new List<SimulationSetting> { 
                GameSettings.Instance.colorR, 
                GameSettings.Instance.colorG, 
                GameSettings.Instance.colorB
            } 
        };

        private SettingGroup MutationGenes = new SettingGroup {
            Settings = new List<SimulationSetting> {
                GameSettings.Instance.mutationChance,
                GameSettings.Instance.mutationVariance,
                GameSettings.Instance.helpfulMutations,

            }
        };

        private SettingGroup DefaultGenes = new SettingGroup { 
            Settings = new List<SimulationSetting> { 
                GameSettings.Instance.diet, 
                GameSettings.Instance.sizeRatio, 
                GameSettings.Instance.speedRatio, 
                GameSettings.Instance.layTime, 
                GameSettings.Instance.broodTime, 
                GameSettings.Instance.hatchTime,
                GameSettings.Instance.strength,
                GameSettings.Instance.viewAngle,
                GameSettings.Instance.viewRadius,
                GameSettings.Instance.clkSpeed,
                GameSettings.Instance.pherosense,
            } 
        };



        private void Start() {
            GameSettings.Instance.pelletCollision.DependantSettings = new List<SimulationSetting> {
                GameSettings.Instance.pelletRotation
            };

            GameSettings.Instance.voidAvoidance.DependantSettings = new List<SimulationSetting> {
                GameSettings.Instance.voidAvoidanceDistance
            };

            GameSettings.Instance.meatRot.DependantSettings = new List<SimulationSetting> {
                GameSettings.Instance.meatRotTime
            };

            GameSettings.Instance.capBibiteGrowth.DependantSettings = new List<SimulationSetting> {
                GameSettings.Instance.capNumber
            };

            SimulationOptions.CreateUIElements(UIReferenceHolder.Instance.SimulationOptionsHolder);
            WorldParameters.CreateUIElements(UIReferenceHolder.Instance.WorldParametersHolder);
            PhysicsParmeters.CreateUIElements(UIReferenceHolder.Instance.PhysicsParametersHolder);

            BibiteConstants.CreateUIElements(UIReferenceHolder.Instance.BibiteEnergyHolder);
            MeatBalance.CreateUIElements(UIReferenceHolder.Instance.MeatBalanceHolder);

            VisualGenes.CreateUIElements(UIReferenceHolder.Instance.VisualGenesHolder);
            MutationGenes.CreateUIElements(UIReferenceHolder.Instance.MutationGenesHolder);
            DefaultGenes.CreateUIElements(UIReferenceHolder.Instance.DefaultGenesHolder);
        }


        public void UpdateControls() {

            SimulationOptions.UpdateUIElements();
            WorldParameters.UpdateUIElements();
            PhysicsParmeters.UpdateUIElements();

            BibiteConstants.UpdateUIElements();
            MeatBalance.UpdateUIElements();

            VisualGenes.UpdateUIElements();
            MutationGenes.UpdateUIElements();
            DefaultGenes.UpdateUIElements();

        }

        public void ResetValues() {

            SimulationOptions.ResetValuesToDefault();
            WorldParameters.ResetValuesToDefault();
            PhysicsParmeters.ResetValuesToDefault();

            BibiteConstants.ResetValuesToDefault();
            MeatBalance.ResetValuesToDefault();

            VisualGenes.ResetValuesToDefault();
            MutationGenes.ResetValuesToDefault();
            DefaultGenes.ResetValuesToDefault();
            


            UpdateControls();
        }
        

        //Functions 
        public void SwitchToPanel(GameObject obj) {
            for(int i = 0; i < Panels.Length; i++) {
                Panels[i].SetActive(false);
            }
            obj.SetActive(true);
        }

        public void UpdateControlsAtStart() {
            StartCoroutine("WaitUpdateControls");
        }

        IEnumerator WaitUpdateControls() {
            LoadingPanel.SetActive(true);
            yield return null;
            for(int i = 0; i < Panels.Length; i++) {
                Panels[i].SetActive(true);
            }
            yield return null;
            UpdateControls();
            yield return null;
            SwitchToPanel(Panels[0]);
            LoadingPanel.SetActive(false);
        }
    }
}
