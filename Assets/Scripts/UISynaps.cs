﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UISynaps : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {
    [SerializeField]
    private float value = 0;
    public Text overText;
    private RawImage img;
    public int index;
    public bool disabled;
    //Transform parent;

    public List<int> siblingSynapses;
    public List<int> siblingNodes;
    public NEATBrain brain;
    public guiManager gM;

    public static float thickness = 0.275f;
    public static float highlightedThickness = 0.425f;


    private void Start() {
        overText.gameObject.SetActive(false);
        img = GetComponent<RawImage>();
        overText.text = "" + Mathf.Round(100f * value) / 100f;
        img.color = getColorGradiant(value);
    }

    public void OnPointerEnter(PointerEventData eventData) {
        overText.gameObject.SetActive(true);

        if(siblingNodes.Count <= 0 || siblingSynapses.Count <= 0) {
            siblingNodes = new List<int>();
            siblingSynapses = new List<int>();
            siblingSynapses = brain.GetConnectedSynapses(brain.synapses[index], new List<int>());
            siblingNodes = brain.GetConnectedNeurons(brain.synapses[index], new List<int>());
        }

        gM.hightlightSubNetwork(siblingSynapses, siblingNodes);
        transform.SetAsLastSibling();
        gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(gameObject.GetComponent<RectTransform>().sizeDelta.x, highlightedThickness + 0.15f);
    }

    public void OnPointerExit(PointerEventData eventData) {
        overText.gameObject.SetActive(false);

        gM.unHightlightSubNetwork(siblingSynapses, siblingNodes);
        transform.SetAsLastSibling();
        gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(gameObject.GetComponent<RectTransform>().sizeDelta.x, thickness);
    }

    public void setValue(float val, bool enab = true) {
        value = val;
        disabled = !enab;
    }

    public void Update() {
        overText.text = "" + Mathf.Round(100f * value) / 100f;
        img.color = getColorGradiant(value);
    }

    private Color getColorGradiant(float val) {
        float gradia = (Mathf.Exp(val) - 1) / (1 + Mathf.Exp(val));
        if(disabled)
            return new Color(1f, 1f, 1f, 0.2f);
        else if(val >= 0)
            return new Color((1 - gradia), 1f, (1 - gradia), 1f);
        else
            return new Color(1f, (gradia + 1), (gradia + 1), 1f);

    }
}
