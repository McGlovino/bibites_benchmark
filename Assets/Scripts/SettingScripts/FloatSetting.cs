﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.UI;
using UnityEngine;
using Assets.Scripts.UIScipts;

namespace Assets.Scripts.SettingScripts {

    [System.Serializable]
    public class FloatSetting : SimulationSetting<float> {

        [System.NonSerialized]
        private SliderSetting slider;

        [System.NonSerialized]
        public float maxValue;
        [System.NonSerialized]
        public float minValue;
        
        [System.NonSerialized]
        public int precision;
        [System.NonSerialized]
        public string prefix;
        [System.NonSerialized]
        public string suffix;

        public override void CreateUIElement(GameObject parent) {
            slider = GameObject.Instantiate(UIReferenceHolder.Instance.FloatSettingPrefab, Vector3.zero, Quaternion.identity, parent.transform).GetComponent<SliderSetting>();

            slider.SetSettingInfo(this);
        }

        public override void updateUIElement() {
            slider.UpdateValue(Value);
        }

        public override void HideUIElement() {
            slider.gameObject.SetActive(false);
        }

        public override void ShowUIElement() {
            slider.gameObject.SetActive(true);

        }

        public override void SetValue(float value) {
            Value = value;
        }
    }

}
