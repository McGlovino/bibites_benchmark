﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.SettingScripts;

public class meatrot : MonoBehaviour {
    private pellet _pellet;
    [SerializeField]
    private PelletSpawner biosys;
    private int timeStart;
    private float rotAmount;

    GameSettings GS;
    TimeStepManager TSM;

    void Awake() {
        _pellet = GetComponent<pellet>();
        biosys = GameObject.FindGameObjectWithTag("FoodSource").GetComponent<PelletSpawner>();

        GS = GameSettings.Instance;
        TSM = TimeStepManager.Instance;

        //variables can be rewritten on load
        timeStart = TSM.totalFrameCount;
        rotAmount = 0f;
    }
    
    
    // Update is called once per frame
    void FixedUpdate() {
        if(GS.meatRot.Value && TSM.totalFrameCount - timeStart > GS.meatRotTime.Value) {
            rotAmount = (TSM.totalFrameCount - timeStart - GS.meatRotTime.Value) / (4 * (1/ Time.fixedDeltaTime));
            _pellet.resize(rotAmount * 0.1f);
            biosys.biomass += rotAmount * 0.1f;
        }
    }
}
