﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Assets.Scripts.UIScipts;

namespace Assets.Scripts.SettingScripts {
    public class IntSetting : SimulationSetting<int>{

        [System.NonSerialized]
        private SliderSetting slider;

        [System.NonSerialized]
        public int maxValue;
        [System.NonSerialized]
        public int minValue;

        [System.NonSerialized]
        public string prefix;
        [System.NonSerialized]
        public string suffix;

        public override void CreateUIElement(GameObject parent) {
            slider = GameObject.Instantiate(UIReferenceHolder.Instance.FloatSettingPrefab, Vector3.zero, Quaternion.identity, parent.transform).GetComponent<SliderSetting>();

            slider.SetSettingInfo(this);
        }

        public override void updateUIElement() {
            slider.UpdateValue(Value);
        }


        public override void HideUIElement() {
            slider.gameObject.SetActive(false);
        }

        public override void ShowUIElement() {
            slider.gameObject.SetActive(true);

        }

        public override void SetValue(int value) {
            Value = value;
        }
    }
}
