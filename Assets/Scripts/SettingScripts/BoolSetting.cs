﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Assets.Scripts.UIScipts;


namespace Assets.Scripts.SettingScripts {
    public class BoolSetting : SimulationSetting<bool> {

        [System.NonSerialized]
        private ToggleSetting toggle;
        
        public override void CreateUIElement(GameObject parent) {
            toggle = GameObject.Instantiate(UIReferenceHolder.Instance.BoolSettingPrefab, Vector3.zero, Quaternion.identity, parent.transform).GetComponent<ToggleSetting>();

            toggle.SetSettingInfo(this);
        }

        public override void SetValue(bool value) {
            Value = value;
            if(DependantSettings != null) {
                if(value) {
                    DependantSettings.ForEach(setting => setting.ShowUIElement());
                }
                else {
                    DependantSettings.ForEach(setting => setting.HideUIElement());
                }
            }
        }

        public override void HideUIElement() {
            toggle.gameObject.SetActive(false);
        }

        public override void ShowUIElement() {
            toggle.gameObject.SetActive(true);
        }

        public override void updateUIElement() {
            toggle.UpdateValue(Value);
            SetValue(Value);
        }

    }
}
