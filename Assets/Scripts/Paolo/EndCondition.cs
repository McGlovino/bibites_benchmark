﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndCondition
{
    float topPercentile = 20;
    int framesAliveExit = 4000;

    List<int> topPercFrames;

    List<BibiteBody> bibites;

    bool inputOverride;

    //Instance
    private static EndCondition EC;
    public static EndCondition Instance
    {
        get
        {
            if (EC == null)
                EC = new EndCondition();
            return EC;
        }
        set
        {
            EC = value;
        }
    }

    void Start()
    {
        EndCondition.Instance = this;

        if (bibites == null)
            bibites = new List<BibiteBody>();
    }

    void FixedUpdate()
    {
        CalculateTopPercFrames();
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
            inputOverride = true;

    }

    void CalculateTopPercFrames()
    {
        List<int> bibitesFrames = new List<int>();
        topPercFrames = new List<int>();

        foreach (BibiteBody bb in bibites)
            bibitesFrames.Add(bb.framesAlive);

        bibitesFrames.Sort(); //lowest to highest
        int splitPoint = Mathf.FloorToInt(bibitesFrames.Count * (1 - (topPercentile / 100)));

        bool allAboveCondition = true;

        for (int i = splitPoint; i < bibitesFrames.Count; i++)
        {
            topPercFrames.Add(bibitesFrames[i]);
            if (bibitesFrames[i] < framesAliveExit)
                allAboveCondition = false;
        }


        if ((topPercFrames.Count > 4 && allAboveCondition) || inputOverride)
        {
            inputOverride = false;

            string rs = "" + topPercFrames[0];
            for (int i = 1; i < topPercFrames.Count; i++)
                rs += "\n" + topPercFrames[i];
            Debug.Log("Condition met: " + allAboveCondition);
            Debug.Log("Top Percentile: " + rs);
        }
    }

    public void AddBibite(BibiteBody bb)
    {
        if (bibites == null)
            bibites = new List<BibiteBody>();
        bibites.Add(bb);
    }

    public void RemoveBibite(BibiteBody bb)
    {
        bibites.Remove(bb);
    }
}
