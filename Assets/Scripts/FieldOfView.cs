﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public class FieldOfView : MonoBehaviour {

    public LayerMask pelletMask;
    public LayerMask bibiteMask;
    
    private List<GameObject> elementsInViewRadius;
    ObjectHandler OH;

    public List<GameObject> bibitesInViewRadius;
    public bool hasHerd;
    private geneCode tarGene;
    public bool needToSee = false;
    public float dist2Bibite;
    public float angletest;
    public float targetR;
    public float targetG;
    public float targetB;

    public float Npellet;
    public float pelletConcentrationAngle;
    public float pelletConcentrationWeight;

    public float NMeat;
    public float meatConcentrationAngle;
    public float meatConcentrationWeight;

    public int Nbibite;
    public float bibiteConcentrationAngle;
    public float bibiteConcentrationWeight;

    public float BibitesHeadingAngle;
    public float BibitesSpeed;

    private Vector3 upvect;
    private Vector2 dir2target;
    private Vector2 herdVelocityVector;
    private float dist2target;

    private float viewRadius;
    private float sizeRatio;
    private float viewAngle;

    void Start() {
        elementsInViewRadius = new List<GameObject>();
        bibitesInViewRadius = new List<GameObject>();

        dist2Bibite = Mathf.Infinity;
        Npellet = 0f;
        NMeat = 0f;
        Nbibite = 0;
        targetR = 0f;
        targetG = 0f;
        targetB = 0f;

        BibitesHeadingAngle = 0f;
        BibitesSpeed = 0f;

        OH = ObjectHandler.Instance;

        StartCoroutine(FindTargetsWithDelay(0.25f));
    }

    public void startSeeing() {
        if(!needToSee) {
            needToSee = true;
            geneCode gene = GetComponent<geneCode>();
            viewAngle = gene.genes[(int)geneCode.BibiteGenes.ViewAngle];
            viewRadius = gene.genes[(int)geneCode.BibiteGenes.ViewRadius];
            sizeRatio = gene.genes[(int)geneCode.BibiteGenes.SizeRatio];
        }
    }

    IEnumerator FindTargetsWithDelay(float delay) {
        while(true) {
            //yield return new WaitForSeconds(delay);
            for (int i = 0; i < (int)(delay * 10); i++)
                yield return new WaitForFixedUpdate();
            FindVisibleTargets();
        }
    }

    void FindVisibleTargets() {
        if(needToSee) {
            dist2Bibite = Mathf.Infinity;
            Npellet = 0f;
            NMeat = 0f;
            Nbibite = 0;
            targetR = 0f;
            targetG = 0f;
            targetB = 0f;
            BibitesHeadingAngle = 0f;
            BibitesSpeed = 0f;

            bibitesInViewRadius.Clear();

            Vector2 bibiteConcentrationVector = Vector2.zero;
            Vector2 pelletConcentrationVector = Vector2.zero;
            Vector2 meatConcentrationVector = Vector2.zero;
            herdVelocityVector = Vector2.zero;

            upvect = transform.up;

            //Physics2D.OverlapCircleNonAlloc(transform.position, viewRadius * sizeRatio * transform.localScale.x, elementsInViewRadius, pelletMask | bibiteMask, 0);
            elementsInViewRadius = OH.objects;
            for(int i = 0; i < elementsInViewRadius.Count; i ++)
            {
                if (Vector2.Distance(transform.position, elementsInViewRadius[i].transform.position) > viewRadius * sizeRatio * transform.localScale.x)
                    elementsInViewRadius.RemoveAt(i);
            }

            // For each item, add to the appropriate ConcentrationVector. Each item's magnitude is based on its
            // direction, size/energy/etc, and divided by distance. So larger or closer items are weighted more.
            for(int i = 0; i < elementsInViewRadius.Count; i++) {
                if(elementsInViewRadius[i] == null)
                    break;
                Transform target = elementsInViewRadius[i].transform;
                dir2target = (target.position - transform.position);
                dist2target = dir2target.magnitude;
                angletest = Vector2.SignedAngle(upvect, dir2target);

                //execute regardless of line of sight
                if(elementsInViewRadius[i].CompareTag("bibite") && elementsInViewRadius[i] != gameObject && elementsInViewRadius[i].GetType() == typeof(CircleCollider2D)) {
                    bibitesInViewRadius.Add(elementsInViewRadius[i]);
                    herdVelocityVector += elementsInViewRadius[i].GetComponent<Rigidbody2D>().velocity;
                }

                //Process direct vision
                if(Mathf.Abs(angletest) < viewAngle / 2) {

                    if(elementsInViewRadius[i].CompareTag("pellet")) {
                        pellet Pelletpellet = elementsInViewRadius[i].GetComponent<pellet>();

                        if(!Pelletpellet.held) {
                            if(!Pelletpellet.isMeat) {
                                Npellet++;
                                pelletConcentrationVector += (dir2target.normalized * Pelletpellet.energy) / dist2target;
                            }
                            else {
                                NMeat++;
                                // TODO: Why is meat energy so low?
                                meatConcentrationVector += (dir2target.normalized * Pelletpellet.energy) / dist2target;
                            }
                        }
                    }
                    else if(elementsInViewRadius[i].CompareTag("bibite") && elementsInViewRadius[i] != gameObject && elementsInViewRadius[i].GetType() == typeof(CircleCollider2D)) {
                        Nbibite++;
                        BibiteBody b = target.GetComponent<BibiteBody>();
                        bibiteConcentrationVector += (dir2target.normalized * b.maxHealth) / dist2target;
                        if(dist2target < dist2Bibite) {

                            tarGene = target.GetComponent<geneCode>();
                            if(tarGene.isReady) {
                                targetR = tarGene.genes[(int)geneCode.BibiteGenes.ColorR];
                                targetG = tarGene.genes[(int)geneCode.BibiteGenes.ColorG];
                                targetB = tarGene.genes[(int)geneCode.BibiteGenes.ColorB];
                            }
                        }
                    }
                }
            }

            // Separate out the magnitude and angle from the vector.
            bibiteConcentrationAngle = Vector2.SignedAngle(upvect, bibiteConcentrationVector);
            bibiteConcentrationWeight = bibiteConcentrationVector.magnitude;

            pelletConcentrationAngle = Vector2.SignedAngle(upvect, pelletConcentrationVector);
            pelletConcentrationWeight = pelletConcentrationVector.magnitude;

            meatConcentrationAngle = Vector2.SignedAngle(upvect, meatConcentrationVector);
            meatConcentrationWeight = meatConcentrationVector.magnitude;
            
            if(bibitesInViewRadius.Count > 0) {
                hasHerd = true;
                BibitesHeadingAngle = Vector2.SignedAngle(upvect, herdVelocityVector);
                BibitesSpeed = Vector2.Dot(herdVelocityVector / bibitesInViewRadius.Count, upvect);
            }
            else {
                hasHerd = false;
                BibitesHeadingAngle = 0f;
                BibitesSpeed = 0f;
            }
        }
    }
}