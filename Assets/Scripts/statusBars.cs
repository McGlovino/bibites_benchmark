﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class statusBars : MonoBehaviour {
    // Start is called before the first frame update

    private GameObject target;

    private float healthRatio;
    [SerializeField]
    private Image healthBar;
    [SerializeField]
    public TextMeshProUGUI healthText;

    private float energyRatio;
    [SerializeField]
    private Image energyBar;
    [SerializeField]
    public TextMeshProUGUI energyText;

    private void Update() {
        if(target != null)
            transform.position = target.transform.position + new Vector3(0, 15f, 0);
    }

    public void targetThat(GameObject GO) {
        if(GO != null) {
            target = GO;
        }
            
        gameObject.SetActive(GO != null);        
    }


    public void updateHealth(float health, float maxHealth) {
        healthRatio = health / maxHealth;
        healthBar.fillAmount = healthRatio;
        healthText.text = $"{Mathf.Round(health)} / {Mathf.Round(maxHealth)}";
    }

    public void updateEnergy(float energy, float maxEnergy) {
        energyRatio = energy / maxEnergy;
        energyBar.fillAmount = energyRatio;
        energyText.text = $"{Mathf.Round(energy)} / {Mathf.Round(maxEnergy)}";
    }
}
