﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using System.Linq;


public class Time_keeper : MonoBehaviour {

    public float simulationStarttime;
    public float simulationOffsettime;

    private List<float> deltatimes = new List<float>();
    const int nFPSSamples = 100;

    public float FPS {
        get {
            return FPSCounter();
        }
    }

    public int hours;
    public int minutes;
    public int seconds;
    public int calc;

    // Use this for initialization
    void Start() {
        simulationStarttime = Time.time;
        simulationOffsettime = 0f;
        
        hours = 0;
        minutes = 0;
        seconds = 0;
        calc = 0;
    }

    // Update is called once per frame
    void Update() {
        calc = Mathf.FloorToInt(Time.time - simulationStarttime + simulationOffsettime);
        seconds = calc % 60;
        calc -= seconds;
        calc /= 60;
        minutes = calc % 60;
        calc -= minutes;
        hours = calc / 60;

        if(deltatimes.Count > nFPSSamples) {
            deltatimes.RemoveAt(0);
        }
        deltatimes.Add(Time.unscaledDeltaTime);
    }

    public float FPSCounter() {
        if(deltatimes != null && deltatimes.Count > 0) {
            float value = 0f;

            for(int i =0; i < deltatimes.Count; i++) {
                value += deltatimes[i];
            }
            return deltatimes.Count / (value);
        }

        return 0f;
    }



}
