﻿using UnityEngine;
using System.Collections;

using Assets.Scripts.SettingScripts;

public class BibiteSpawner : MonoBehaviour {
    private Vector3 randPos;
    public GameObject Entity;
    public int nEntity;
    public float wait;
    public float spawnBoundaries;
    public geneCode gene;
    public srcGene srcgene;
    private GameObject spawned;
    private geneCode eggGene;
    public float energystart;
    private NEATBrain baseBrain;
    private NEATBrain spawnedbrain;
    private guiManager gui;
    public bool flag;
    private PelletSpawner pelletSpawner;
    private GameObject bibiteHolder;

    private GameObject[] count;

    // Use this for initialization
    public void StartSpawner() {
        flag = true;
        pelletSpawner = GameObject.FindGameObjectWithTag("FoodSource").GetComponent<PelletSpawner>();
        bibiteHolder = WorldObjectsSpawner.BibiteHolder;
        nEntity = 1;
        gene = GetComponent<geneCode>();
        baseBrain = GetComponent<NEATBrain>();

        InitGenesFromGamesettings();

        energystart = gene.getLayEnergy();

        baseBrain.parent = false;
        baseBrain.nHidden = 0;
        baseBrain.nSynaps = 0;

        baseBrain.StartBrain();
        baseBrain.initBrain();

        spawned = GameObject.Find("interfaceManager");
        gui = spawned.GetComponent<guiManager>();
        StartCoroutine(spawnEntity(2f));
    }

    IEnumerator spawnEntity(float delay) {
        float bibiteToSpawn = 0f;
        while(true) {
            bibiteToSpawn += GameSettings.Instance.bibiteGrowth.Value * (delay/10);
            while(bibiteToSpawn >= 1f) {
                if((!GameSettings.Instance.capBibiteGrowth.Value || nEntity < GameSettings.Instance.capNumber.Value)) {

                    if(pelletSpawner.biomass < energystart) {
                        pelletSpawner.recyclePlants(energystart);
                    }

                    spawned = Instantiate(Entity, pelletSpawner.GetSpawnPoint(), Quaternion.identity, bibiteHolder.transform) as GameObject;

                    eggGene = spawned.GetComponent<geneCode>();
                    eggHatch egghatch = spawned.GetComponent<eggHatch>();
                    egghatch.energy = energystart;
                    pelletSpawner.biomass -= energystart;
                    spawnedbrain = spawned.GetComponent<NEATBrain>();

                    eggGene.copyGene(gene, increaseGeneration:true);
                    eggGene.mutate((int)gene.genes[(int)geneCode.BibiteGenes.AverageMutationNumber]);

                    spawnedbrain.copyBrain(baseBrain, true, true);
                    bibiteToSpawn -= 1f;
                    
                    egghatch.StartHatch();
                }
                else {
                    break;
                }
            }

            for (int i = 0; i < (int)(delay * 10); i++)
                yield return new WaitForFixedUpdate();
            //yield return new WaitForSeconds(delay);
        }
    }


    void InitGenesFromGamesettings() {
        gene.StartGenes();

        gene.genes[(int)geneCode.BibiteGenes.LayTime] =                 GameSettings.Instance.layTime.Value;
        gene.genes[(int)geneCode.BibiteGenes.BroodTime] =               GameSettings.Instance.broodTime.Value;
        gene.genes[(int)geneCode.BibiteGenes.HatchTime] =               GameSettings.Instance.hatchTime.Value;
        gene.genes[(int)geneCode.BibiteGenes.SizeRatio] =               GameSettings.Instance.sizeRatio.Value;
        gene.genes[(int)geneCode.BibiteGenes.SpeedRatio] =              GameSettings.Instance.speedRatio.Value;
        gene.genes[(int)geneCode.BibiteGenes.ColorR] =                  GameSettings.Instance.colorR.Value;
        gene.genes[(int)geneCode.BibiteGenes.ColorG] =                  GameSettings.Instance.colorG.Value;
        gene.genes[(int)geneCode.BibiteGenes.ColorB] =                  GameSettings.Instance.colorB.Value;
        gene.genes[(int)geneCode.BibiteGenes.Strength] =                GameSettings.Instance.strength.Value;
        gene.genes[(int)geneCode.BibiteGenes.MutationAmountSigma] =     GameSettings.Instance.mutationVariance.Value;
        gene.genes[(int)geneCode.BibiteGenes.AverageMutationNumber] =   GameSettings.Instance.mutationChance.Value;
        gene.genes[(int)geneCode.BibiteGenes.ViewAngle] =               GameSettings.Instance.viewAngle.Value;
        gene.genes[(int)geneCode.BibiteGenes.ViewRadius] =              GameSettings.Instance.viewRadius.Value;
        gene.genes[(int)geneCode.BibiteGenes.ClockSpeed] =              GameSettings.Instance.clkSpeed.Value;
        gene.genes[(int)geneCode.BibiteGenes.PheroSense] =              GameSettings.Instance.pherosense.Value;
        gene.genes[(int)geneCode.BibiteGenes.Diet] =                    GameSettings.Instance.diet.Value;
    }

}
