﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Scripts.SettingScripts { 

    public class SettingHelper : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler 
        {
        public GameObject HelperPanel;
        public string WikiLink;



        public void OnPointerEnter(PointerEventData eventData) {
            if(HelperPanel != null) {
                HelperPanel.SetActive(true);
            }
        }

        public void OnPointerExit(PointerEventData eventData) {
            if(HelperPanel != null) {
                HelperPanel.SetActive(false);
            }
        }

        public void OpenWikiLink() {
            Application.OpenURL("https://the-bibites.fandom.com/wiki/" + WikiLink);

        }


    }
}
