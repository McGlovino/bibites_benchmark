﻿using System;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;
using Assets.Scripts.SettingScripts;
using System.Collections.Generic;

//TODO: biomass management should be moved out from PelletSpawner to separate component
public class PelletSpawner : MonoBehaviour
{
    [SerializeField] public float spawnTime;
    [SerializeField] public float biomass;

    //TODO: remove Fields because it is unused now;
    public float avgSize;


    private float initialBiomass;
    private GameObject pelletHolder;

    private pellet spawnedPellet;
    private Vector3 randPos;
    public GameObject Entity;
    private GameObject Spawner;
    private guiManager gui;
    private BibiteSpawner eSpawn;
    public int nEntity;
    public float spawnBoundaries;
    private GameObject[] count;

    [SerializeField] private int numSpawnCenters;
    [SerializeField] private Vector3[] spawnCenters;
    [SerializeField] private float spawnRadius;
    public BinCounter[] InputLoggers;

    [SerializeField] public bool initialSpawnComplete = false;
    private void Awake()
    {
        Spawner = GameObject.FindGameObjectWithTag("EggSpawner");
        eSpawn = Spawner.GetComponent<BibiteSpawner>();
        Spawner = GameObject.Find("interfaceManager");
        gui = Spawner.GetComponent<guiManager>();

#if DEBUG
        // Create loggers.
        System.Array inputs = System.Enum.GetValues(typeof(NEATBrain.Inputs));
        InputLoggers = new BinCounter[inputs.Length + 1];

        // TODO: Change the input neurons to be class based to have properties like min/max, then this can just iterate.
        InputLoggers[(int)NEATBrain.Inputs.AttackedDamage] = new BinCounter(25, 0f, 3f);
        InputLoggers[(int)NEATBrain.Inputs.BibiteConcentrationAngle] = new BinCounter(50, -3f, 3f);
        InputLoggers[(int)NEATBrain.Inputs.BibiteConcentrationWeight] = new BinCounter(50, 0f, 2f);
        InputLoggers[(int)NEATBrain.Inputs.MeatConcentrationAngle] = new BinCounter(50, -3f, 3f);
        InputLoggers[(int)NEATBrain.Inputs.MeatConcentrationWeight] = new BinCounter(50, 0f, 2f);
        InputLoggers[(int)NEATBrain.Inputs.PelletConcentrationAngle] = new BinCounter(50, -3f, 3f);
        InputLoggers[(int)NEATBrain.Inputs.PelletConcentrationWeight] = new BinCounter(50, 0f, 2f);
        InputLoggers[(int)NEATBrain.Inputs.ClosestBibiteB] = new BinCounter(50, 0f, 1f);
        InputLoggers[(int)NEATBrain.Inputs.ClosestBibiteG] = new BinCounter(50, 0f, 1f);
        InputLoggers[(int)NEATBrain.Inputs.ClosestBibiteR] = new BinCounter(50, 0f, 1f);
        InputLoggers[(int)NEATBrain.Inputs.EnergyRatio] = new BinCounter(50, 0f, 5f);
        InputLoggers[(int)NEATBrain.Inputs.LifeRatio] = new BinCounter(25, 0f, 1.1f);
        InputLoggers[(int)NEATBrain.Inputs.Maturity] = new BinCounter(50, 0f, 3f);
        InputLoggers[(int)NEATBrain.Inputs.TimeAlive] = new BinCounter(100, 0f, 10f);
        InputLoggers[(int)NEATBrain.Inputs.Minute] = new BinCounter(100, 0f, 10f);
        InputLoggers[(int)NEATBrain.Inputs.NVisibleBibites] = new BinCounter(21, 0f, 2f);
        InputLoggers[(int)NEATBrain.Inputs.NVisibleMeat] = new BinCounter(21, 0f, 3f);
        InputLoggers[(int)NEATBrain.Inputs.NVisiblePellets] = new BinCounter(21, 0f, 5f);
        InputLoggers[(int)NEATBrain.Inputs.Phero1Angle] = new BinCounter(50, -3f, 3f);
        InputLoggers[(int)NEATBrain.Inputs.Phero2Angle] = new BinCounter(50, -3f, 3f);
        InputLoggers[(int)NEATBrain.Inputs.Phero3Angle] = new BinCounter(50, -3f, 3f);
        InputLoggers[(int)NEATBrain.Inputs.PheroSense1] = new BinCounter(50, 0f, 4f);
        InputLoggers[(int)NEATBrain.Inputs.PheroSense2] = new BinCounter(50, 0f, 4f);
        InputLoggers[(int)NEATBrain.Inputs.PheroSense3] = new BinCounter(50, 0f, 4f);
        InputLoggers[(int)NEATBrain.Inputs.Speed] = new BinCounter(50, -2f, 2f);
#endif
    }

    public void ResumeSpawner()
    {
        initialBiomass = GameSettings.Instance.biomassDensity.Value * GameSettings.Instance.SimulationSize.Value * GameSettings.Instance.SimulationSize.Value;

        pelletHolder = WorldObjectsSpawner.PelletHolder;

        StartCoroutine(spawnEntity(spawnTime));
        StartCoroutine(countBiomass(1f));
    }

    public void StartSpawner()
    {
        nEntity = 0;

        // Experimenation showed we want about 8 minimum centers (at the min of 1000 size), then about 6 per additional 1000 extent. 
        numSpawnCenters = (int)(2 + 6 * (spawnBoundaries / 1000));

        spawnCenters = new Vector3[numSpawnCenters];

        // The more numSpawnCenters, the tighter they are. Don't make too tight or things get really slow because of collisions
        spawnRadius = 2f * spawnBoundaries / numSpawnCenters;

        float startBounds = spawnBoundaries - spawnRadius;
        for (var i = 0; i < spawnCenters.Length; i++)
        {
            spawnCenters[i] = new Vector3(Random.Range(-startBounds, startBounds),
                Random.Range(-startBounds, startBounds), 0);
        }

        ResumeSpawner();
    }

    public Vector3 GetSpawnPoint()
    {
        // Pick an existing spawn point, and then move randomly out from there, 
        int target = Random.Range(0, spawnCenters.Length);
        Vector3 move = Random.insideUnitCircle * spawnRadius;
        Vector3 randPos = spawnCenters[target] + move;

        // Move the spawn center a bit toward the new location. 
        // This results in the environment changing over time, and more interesting looking fields of plants
        Vector3 moveTo = spawnCenters[target] + (move * .2f);
        if (Mathf.Abs(moveTo.x) < spawnBoundaries && Mathf.Abs(moveTo.y) < spawnBoundaries)
        {
            spawnCenters[target] = moveTo;
        }

        return randPos;
    }

    IEnumerator spawnEntity(float delay)
    {
        float tempBio = 0f;
        int cnt = 0;
        float trgtN = 0.85f * (biomass - 5000) / GameSettings.Instance.pelletSize.Value;

        while (true)
        {
            if (biomass > 0)
            {
                cnt = 1;

                if (!initialSpawnComplete)
                {
                    cnt = (int)trgtN;
                    initialSpawnComplete = true;
                }

                if (biomass < 5000)
                {
                    cnt = 0;
                }

                for (int i = 0; i < cnt; i++)
                {
                    Spawner = Instantiate(Entity, GetSpawnPoint(), Quaternion.identity, pelletHolder.transform);
                    spawnedPellet = Spawner.GetComponent<pellet>();
                    tempBio = Random.Range(0.75f * GameSettings.Instance.pelletSize.Value, 1.25f * GameSettings.Instance.pelletSize.Value);

                    if (tempBio > biomass)
                        tempBio = biomass;


                    spawnedPellet.initializePellet(tempBio);
                    biomass -= tempBio;
                }
            }
            for (int i = 0; i < (delay * 10); i++)
                yield return new WaitForFixedUpdate();
        }
    }

    IEnumerator countBiomass(float delay)
    {
        while (true)
        {
            for (int i = 0; i < (int)(delay * 10); i++)
                yield return new WaitForFixedUpdate();
            UpdateBiomass();
        }
    }

    public void UpdateBiomass()
    {
        float pelletBiomass = 0f;
        float bibiteBiomass = 0f;
        float eggBiomass = 0f;
        float plantBiomass = 0f;
        float meatBiomass = 0f;
        BibiteBody bibito;
        eggHatch eggo;
        int eggCount = 0;
        int plantCount = 0;
        int meatCount = 0;
        int bibiteCount = 0;


        //Measuring approximateley the plant energy (measuring exactly might be too costyl considering the number of instances)
        plantCount = pellet.count;
        plantBiomass = (float)pellet.plantBiomass;

        meatCount = pellet.meatCount;
        meatBiomass = (float)pellet.meatBiomass;

        pelletBiomass = meatBiomass + plantBiomass;

        //Measuring exactly the total biomass in bibites and eggs
        foreach (Transform child in WorldObjectsSpawner.BibiteHolder.transform)
        {
            if (child.gameObject.CompareTag("bibite"))
            {
                bibito = child.GetComponent<BibiteBody>();
                bibiteCount++;
                bibiteBiomass += bibito.totalEnergy;
            }
            else if (child.gameObject.CompareTag("egg"))
            {
                eggo = child.GetComponent<eggHatch>();
                eggCount++;
                eggBiomass += eggo.energy;
            }
        }

        // Note this biomass value is used outside this function for other reasons. 
        biomass = initialBiomass - pelletBiomass - bibiteBiomass - eggBiomass;

        gui.updateBiomass(plantCount, meatCount, bibiteCount, eggCount, this.biomass, pelletBiomass, bibiteBiomass,
            eggBiomass, plantBiomass, meatBiomass);
    }

    public void recyclePlants(float amount)
    {

        foreach (Transform child in WorldObjectsSpawner.PelletHolder.transform)
        {

            if (biomass >= amount)
            {
                break;
            }

            var pellet = child.gameObject.GetComponent<pellet>();

            biomass += pellet.resize(amount - biomass);

        }
    }
}