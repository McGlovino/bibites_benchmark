﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace RandomExtansion {
    public static class RandomExtension {
        public static float NextGaussian(this System.Random r, float mu = 0f, float sig = 0f) {
            var u1 = 1.0 - r.NextDouble();
            var u2 = 1.0 - r.NextDouble();

            var rand_std_normal = Math.Sqrt(-2.0 * Math.Log(u1)) *
                                Math.Sin(2.0 * Math.PI * u2);

            var rand_normal = mu + sig * rand_std_normal;

            return (float)rand_normal;
        }

        public static float NextRelativeGaussianMutation(this System.Random r, float geneValue = 1f, float mutationSigma = 0f) {
            float u = r.NextGaussian(0f, 1f);
            float v = Mathf.Pow(1f + mutationSigma, u);
            return geneValue*(v-1f);
        }

        public static int NextPoisson(this System.Random r, double lambda = 1.0) {

            double L = Math.Exp(-lambda);
            double p = 1;
            int k = 0;
            double u;
            do {
                k = k + 1;
                u = r.NextDouble();
                p = p * u;
            } while(p > L);


            return k-1;

        }

    }
}

