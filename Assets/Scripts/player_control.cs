﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.SettingScripts;

public class player_control : MonoBehaviour {
    public GameObject egg;
    public GameObject meat;
    public pellet pellet;
    public player_control other;
    public int numberOfLinks;
    public Rigidbody2D rb2d;
    private geneCode gene;
    private NEATBrain brain;
    internal internClock clk;
    private FixedJoint2D lovedOne;
    private BibiteBody body;
    private BibiteGrowth growth;
    private FieldOfView fov;

    internal float signedVelocity;
    internal float angle = 0;
    internal float metabolism = 0;
    private float layEnergy = 0;
    public float attackedDmg = 0;

    // creating
    private GameObject PheroSystem;
    private pheromones spawnedPheromones;
    public GameObject Pherosrc;
    private GameObject bibiteHolder;
    private GameObject pheromonesHolder;

    private int lienN;

    private float _targetdist;
    private Vector2 move;
    private float musclePower;
    private geneCode eggGene;
    private geneCode gene2;

    private NEATBrain brain2;
    private NEATBrain eggBrain;

    private GameObject spawnedEgg;
    bool parent2;
    public bool canDie;

    public void StartControling() {
        rb2d = GetComponent<Rigidbody2D>();
        gene = GetComponent<geneCode>();
        body = GetComponent<BibiteBody>();
        growth = GetComponent<BibiteGrowth>();
        brain = GetComponent<NEATBrain>();
        clk = GetComponent<internClock>();
        fov = GetComponent<FieldOfView>();

        bibiteHolder = WorldObjectsSpawner.BibiteHolder;
        pheromonesHolder = WorldObjectsSpawner.PheromoneHolder;

        numberOfLinks = 0;
        parent2 = false;

        musclePower = GameSettings.Instance.forwardForce.Value * gene.genes[(int)geneCode.BibiteGenes.SpeedRatio] * body.rootSize;

        //1f (max Health) + (max energy) + 1/x for health
        layEnergy = gene.getLayEnergy();


        rb2d.velocity = new Vector2(0f, 0f);
        StartCoroutine(PheroProduceWithDelay(0.875f));
        canDie = true;
    }

    internal IEnumerator layEggs() {
        
        float period = gene.genes[(int)geneCode.BibiteGenes.BroodTime] * gene.genes[(int)geneCode.BibiteGenes.SizeRatio];
        GameObject bibiteHolder = WorldObjectsSpawner.BibiteHolder;
        //Debug.Log(TimeStepManager.Instance.totalFrameCount);
        while(true) {
            //yield return new WaitForSeconds(period);
            for (int i = 0; i < (int)(period * 10); i++)
                yield return new WaitForFixedUpdate();

            if (brain.outs[(int)NEATBrain.Outputs.Want2Lay] >= 0.25f && layEnergy <= body.energy && body.healthRatio > 0.5f) {
                body.useEnergy(LayEgg());
            }
        }
    }

    public float LayEgg() {
        spawnedEgg = Instantiate(egg, transform.position, Quaternion.identity, bibiteHolder.transform) as GameObject;
        eggGene = spawnedEgg.GetComponent<geneCode>();
        eggBrain = spawnedEgg.GetComponent<NEATBrain>();
        eggHatch egghatch = spawnedEgg.GetComponent<eggHatch>();
        egghatch.energy = layEnergy;

        eggGene.copyGene(gene, increaseGeneration: true);
        eggGene.mutate();
        eggBrain.copyBrain(brain, mutate: true);
        egghatch.StartHatch();

        return layEnergy;
    }

    IEnumerator PheroProduceWithDelay(float delay) {
        GameObject pheromonesHolder = WorldObjectsSpawner.PheromoneHolder;
        while(true) {
            //yield return new WaitForSeconds(delay);
            for (int i = 0; i < (int)(delay * 10); i++)
                yield return new WaitForFixedUpdate();

            if (brain.outs[(int)NEATBrain.Outputs.PhereOut1] > 0.01f ||
               brain.outs[(int)NEATBrain.Outputs.PhereOut2] > 0.01f ||
               brain.outs[(int)NEATBrain.Outputs.PhereOut3] > 0.01f) {
                PheroSystem = Instantiate(Pherosrc, transform.position, Quaternion.identity, pheromonesHolder.transform) as GameObject;
                spawnedPheromones = PheroSystem.GetComponent<pheromones>();
                spawnedPheromones.setStrength(brain.outs[(int)NEATBrain.Outputs.PhereOut1],
                    brain.outs[(int)NEATBrain.Outputs.PhereOut2], brain.outs[(int)NEATBrain.Outputs.PhereOut3]);
                spawnedPheromones.startPheros();
                body.useEnergy((brain.outs[(int)NEATBrain.Outputs.PhereOut1] +
                                brain.outs[(int)NEATBrain.Outputs.PhereOut2] +
                                brain.outs[(int)NEATBrain.Outputs.PhereOut3]) * GameSettings.Instance.pheromoneProductionCost.Value * delay) ;
            }
        }
    }

    //Called from the BibiteBody script
    public void actions() {
        //get self awareness
        Vector2 velocity = rb2d.velocity;
        Vector2 dir = transform.up;


        signedVelocity = Vector2.Dot(velocity, dir);
        angle = Vector2.Angle(velocity, Vector2.right);


        Vector3 cross = Vector3.Cross(velocity, Vector2.right);
        if(cross.z > 0)
            angle = 360 - angle;

        float acc = brain.outs[(int)NEATBrain.Outputs.Accelerate] * 10;
        if(double.IsNaN(acc)) acc = 0;

        float rotate = brain.outs[(int)NEATBrain.Outputs.Rotate] * 10;
        if(double.IsNaN(rotate)) rotate = 0;

        
        // accelaration and rotation are proportianally weigthed to match herd speed depending on herding behavior
        if(fov.hasHerd) {
            float herding = brain.outs[(int)NEATBrain.Outputs.Herding];
            acc = acc * (1f - Mathf.Abs(herding)) + (float)System.Math.Tanh(fov.BibitesSpeed - signedVelocity) * herding;
            rotate = rotate * (1f - Mathf.Abs(herding)) + (float)System.Math.Tanh(fov.BibitesHeadingAngle) * herding;
        }

        //If Void Avoidance is turned On, overwrite wants
        if(GameSettings.Instance.voidAvoidance.Value) {
            float factor = 0f;

            if(Mathf.Abs(transform.position.x) > GameSettings.Instance.SimulationSize.Value + 100f) {
                factor += (Mathf.Abs(transform.position.x) - GameSettings.Instance.SimulationSize.Value - 100f) / GameSettings.Instance.voidAvoidanceDistance.Value;
            }

            if(Mathf.Abs(transform.position.y) > GameSettings.Instance.SimulationSize.Value + 100f ) {
                factor += (Mathf.Abs(transform.position.y) - GameSettings.Instance.SimulationSize.Value - 100f) / GameSettings.Instance.voidAvoidanceDistance.Value;
            }

            if(factor > 0f) {
                factor = factor > 1f ? 1f : factor;

                rotate = rotate * (1f - factor) + (float)System.Math.Tanh(Vector2.SignedAngle(dir,-transform.position)) * factor;
            }
        }


        body.useEnergy(GameSettings.Instance.moveCost.Value * Mathf.Abs(acc) * 0.1f);

        if(acc < 0) acc *= GameSettings.Instance.backwardFraction.Value;
        rb2d.AddForce(transform.up * (this.musclePower * acc * body.rootSize));

        
        rb2d.AddTorque(rotate * this.musclePower / 2 * body.rootSize);

        body.useEnergy(Mathf.Abs(rotate) * 0.1f * GameSettings.Instance.moveCost.Value / 2f);

        //Liens
        if(numberOfLinks > 0) {
            if(brain.outs[(int)NEATBrain.Outputs.Want2Eat] >= 0.75) {
                foreach(FixedJoint2D join in gameObject.GetComponents<FixedJoint2D>()) {
                    if(join.connectedBody == null)
                        Destroy(join);
                    else if(join != null && join.connectedBody.CompareTag("pellet"))
                        eat(join.connectedBody.GetComponent<pellet>());
                }
            }

            if(brain.outs[(int)NEATBrain.Outputs.Grab] <= 0.25)
                removeAllJoins();
        }

        //holding someone
        if(brain.outs[(int)NEATBrain.Outputs.Grab] <= 0.75 & parent2) {
            other.breakLove();
            breakLove();
        }
    }

    public void removeAllJoins() {
        if(numberOfLinks > 0) {
            foreach(FixedJoint2D join in gameObject.GetComponents<FixedJoint2D>()) {
                if(join != null) {
                    if(join.connectedBody != null) {
                        if(join.connectedBody.CompareTag("pellet"))
                            join.connectedBody.GetComponent<pellet>().held = false;
                    }
                }
                Destroy(join);
            }
            numberOfLinks = 0;
        }
    }


    /// <summary>
    /// This handles both attacking and sex
    /// </summary>
    /// <param name="other"></param>
    public void OnTriggerEnter2D(Collider2D other) {
        if(!other.isTrigger && other.gameObject.CompareTag("bibite")) {
            player_control otherBibit = other.gameObject.GetComponent<player_control>();

            if(brain.outs[(int)NEATBrain.Outputs.Want2Sex] >= 0.75f &&
               otherBibit.brain.outs[(int)NEATBrain.Outputs.Want2Sex] >= 0.75f &&
               !parent2 && !otherBibit.parent2) {
                creerLove(other.gameObject);
                otherBibit.creerLove(this.gameObject);
            }
            else {
                maybeAttack(otherBibit.gameObject, false);
            }
        }
    }

    void OnTriggerStay2D(Collider2D other) {
        if(other.gameObject.CompareTag("pellet")) {
            pellet = other.gameObject.GetComponent<pellet>();

            if(brain.outs[(int)NEATBrain.Outputs.Grab] >= 0.75) {
                if(!pellet.held) {
                    creerJoin(other.gameObject.GetComponent<Rigidbody2D>());
                    pellet.catched(this);
                }
            }

            if(brain.outs[(int)NEATBrain.Outputs.Want2Eat] >= 0.5)
                eat(pellet);
        }
        else if(other.gameObject.CompareTag("egg")) {
            // TODO: change to differentiate between eggs, meat, and pellets. It seems like carnivores should try meat and eggs. 
        }
        else if(other.gameObject.CompareTag("bibite")) {
            maybeAttack(other.gameObject, true);
        }
    }

    void maybeAttack(GameObject otherBibite, bool ongoingAttack) {
        BibiteBody otherBody = otherBibite.GetComponent<BibiteBody>();

        //other bibite is not ready to interact with
        if(otherBody.born == false) {
            return;
        }

        float strength = body.getStrength();
        float otherStrength = otherBody.getStrength();

        if(brain.outs[(int)NEATBrain.Outputs.Want2Attack] >= 0.25f && (strength - otherStrength) >= -2f) {
            float attemptedAmount = 5f * (strength - otherStrength + 2) * body.rootSize;
            attemptedAmount /= ongoingAttack ? 5 : 1;

            float energyGain = otherBody.Attacked(attemptedAmount);

            if(brain.outs[(int)NEATBrain.Outputs.Want2Eat] >= 0.5f) {
                float dietEfficiency = -0.3f + gene.genes[(int)geneCode.BibiteGenes.Diet];
                body.gainEnergy(energyGain * dietEfficiency, true);
            }

            if(otherBody.health > 0) {
                otherBibite.GetComponent<Rigidbody2D>()
                    .AddForce(
                        Mathf.Sqrt(body.size * 150 * strength) *
                        (otherBibite.transform.position - transform.position).normalized, ForceMode2D.Impulse);
            }
        }
    }

    void creerJoin(Rigidbody2D cible) {
        var tempLink = gameObject.AddComponent<FixedJoint2D>();
        tempLink.connectedBody = cible;
        numberOfLinks++;
    }

    internal void creerLove(GameObject love) {
        parent2 = true;
        other = love.GetComponent<player_control>();
        lovedOne = gameObject.AddComponent<FixedJoint2D>();
        lovedOne.connectedBody = love.GetComponent<Rigidbody2D>();
    }

    internal void breakLove() {
        Destroy(lovedOne);
        lovedOne = null;
        other = null;
        parent2 = false;
    }

    void eat(pellet food) {

        float maxE = GameSettings.Instance.maxDietEfficiency.Value;
        float minE = GameSettings.Instance.minDietEfficiency.Value;
        float dietEfficiency;
        if(food.isMeat) {
            dietEfficiency = minE + (maxE-minE)*gene.genes[(int)geneCode.BibiteGenes.Diet];
        }
        else {
            dietEfficiency = maxE + (minE-maxE)*gene.genes[(int)geneCode.BibiteGenes.Diet];
        }

        if(Mathf.Approximately(dietEfficiency, 0f)) {
            //TODO Fix the energy leaks
            food.resize(food.energy);
        }
        else if(dietEfficiency > 0f) {
            food.resize(body.gainEnergy(food.energy * dietEfficiency, false) / dietEfficiency);
        }
        else {
            body.useEnergy(Mathf.Abs(food.energy * dietEfficiency), false);
            food.resize(food.energy);
        }
    }

}