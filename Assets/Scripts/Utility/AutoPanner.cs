﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.UtilityScripts {
    public class AutoPanner : MonoBehaviour {
        // Start is called before the first frame update

        public float force = 10f;
        public float maxSpeed = 1f;
        public int minWait = 30;
        public int maxWait = 90;
        [SerializeField]
        private PelletSpawner ps = null;

        private Vector3 destination;
        private Rigidbody2D rb;
        private bool started = false;

        void Awake() {
            rb = GetComponent<Rigidbody2D>();
        }

        public void StartAutoPanner(PelletSpawner _ps) {
            ps = _ps;
            started = true;
            StartCoroutine(ChangeDirection());
        }

        // Update is called once per frame
        void FixedUpdate() {
            if(started) {
                Vector3 dir = destination - transform.position;

                if(dir.magnitude < 10f)
                    NewDir();

                rb.AddForce(force * dir.normalized * (maxSpeed - rb.velocity.magnitude));
            }
        }

        IEnumerator ChangeDirection() {

            int wait = Random.Range(minWait, maxWait);

            while(true) {
                //yield return new WaitForSeconds(wait);
                for (int i = 0; i < (int)(wait * 10); i++)
                    yield return new WaitForFixedUpdate();

                NewDir();

                wait = Random.Range(minWait, maxWait);
            }
        }

        void NewDir() {
            destination = ps.GetSpawnPoint();
        }
    }
}