﻿using System;
using UnityEngine;
using System.Collections;

using Assets.Scripts.SettingScripts;

public class pellet : MonoBehaviour {
    static public int count = 0;
    static public int meatCount = 0;
    static public double plantBiomass = 0f;
    static public double meatBiomass = 0f;

    [SerializeField]
    public float energy;
    public bool held;
    private Rigidbody2D rb;
    public player_control attachedTo;
    public int numberOfLinks;

    GameSettings GS;
    ObjectHandler OH;
    
    [SerializeField]
    public bool isMeat;

    private void Awake() {
        rb = GetComponent<Rigidbody2D>();

        GS = GameSettings.Instance;
        OH = ObjectHandler.Instance;
        OH.AddObject(gameObject);
    }

    public static void ResetCount() {
        count = 0;
        meatCount = 0;
        plantBiomass = 0f;
        meatBiomass = 0f;
    }

    public void addPelletToGlobalCount() {
        if(!isMeat) {
            count++;
            plantBiomass += energy;
        }
        else {
            meatCount++;
            meatBiomass += energy ;
        }
    }

    public void removePelletBiomass(float amount) {
        if(!isMeat)
            plantBiomass -= amount;
        else
            meatBiomass -= amount;

        energy -= amount;
    }

    public void initializePellet(float size) {
        energy = size;

        addPelletToGlobalCount();

        float sizeFactor = energy / GS.pelletSize.Value;
        transform.localScale = Vector3.one * Mathf.Sqrt(sizeFactor);

        if(!GS.pelletCollision.Value) {
            var col = GetComponent<Collider2D>();
            col.isTrigger = true;
        }
        else {
            rb.mass = sizeFactor * GS.defaultPelletMass.Value;
            rb.freezeRotation = !GS.pelletRotation.Value;
        }

    }

    public float resize(float amountToRemove) {
        
        energy = energy > 0f ? energy : 0.0001f;

        if(energy <= amountToRemove) {

            float prevEnergy = energy;
            removePelletBiomass(energy);

            OH.RemoveObject(gameObject);
            Destroy(gameObject);

            return prevEnergy;
        }
        else {

            removePelletBiomass(amountToRemove);

            float sizeFactor = energy / GS.pelletSize.Value;
            transform.localScale = Vector3.one * Mathf.Sqrt(sizeFactor);

            if(rb != null) {
                rb.mass = GS.defaultPelletMass.Value * sizeFactor;
            }

            return amountToRemove;
        }
    }

    public void catched(player_control vi) {
        held = true;
        attachedTo = vi;
        numberOfLinks = attachedTo.numberOfLinks - 1;
    }

    private void OnDestroy() {
        if(!isMeat) {
            plantBiomass -= energy;
            count--;
        }
        else {
            meatBiomass -= energy;
            meatCount--;
        }
    }
}
