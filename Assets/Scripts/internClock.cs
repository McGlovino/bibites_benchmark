﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class internClock : MonoBehaviour {

    private geneCode gene;
    private NEATBrain brain;

    [SerializeField]
    internal int tic;
    [SerializeField]
    private float ticProgress = 0;

    [SerializeField]
    internal float timeAlive;

    [SerializeField]
    internal float chronoTime;


    private float period;

    void InitClock() {
        gene = GetComponent<geneCode>();
        brain = GetComponent<NEATBrain>();
    }

    // Use this for initialization
    public void StartClock() {
        InitClock();

        tic = 0;
        ticProgress = 0f;

        timeAlive = 0f;

        chronoTime = 0f;

        StartCoroutine(waitReady());
    }

    public void ResumeClock() {
        InitClock();

        StartCoroutine(waitReady());
    }

    IEnumerator waitReady() {

        while(!brain.isReady) {
            //yield return new WaitForSeconds(0.1f);
            yield return new WaitForFixedUpdate();
        }

        period = gene.genes[(int)geneCode.BibiteGenes.ClockSpeed];
        StartCoroutine(tick(0.1f <= period ? 0.1f : period));
    }

    IEnumerator tick(float delay) {
        
        while(true) {
            //yield return new WaitForSeconds(delay);
            for (int i = 0; i < (int)(delay * 10); i++)
                yield return new WaitForFixedUpdate();

            ticProgress += delay;
            if(ticProgress >= period) {
                ticProgress -= period;
                tic = 1- tic;
            }

            if(brain.outs[(int)NEATBrain.Outputs.ClkReset] > 0.75f)
                chronoTime = 0;
            else
                chronoTime += delay;
               
            timeAlive += delay;
        }
    }
}
