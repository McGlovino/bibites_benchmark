﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ErrorDisplay : MonoBehaviour, ISerializationCallbackReceiver {
    public TextMeshProUGUI errorSourceText;
    public TextMeshProUGUI errorText;
    public GameObject errorPanel;

    static TextMeshProUGUI ErrorSourceText;
    static TextMeshProUGUI ErrorText;
    static GameObject ErrorPanel;

    public void OnAfterDeserialize() {
        ErrorSourceText = errorSourceText;
        ErrorText = errorText;
        ErrorPanel = errorPanel;
    }

    public void OnBeforeSerialize() { }

    public static void ShowError(string source, string text) {
        ErrorPanel.SetActive(true);
        ErrorSourceText.text = source;
        ErrorText.text = text;
        UnityEngine.UI.LayoutRebuilder.ForceRebuildLayoutImmediate(ErrorPanel.GetComponent<RectTransform>());
    }
}
