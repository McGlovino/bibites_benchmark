﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Assets.Scripts.SettingScripts;
using UnityEngine.UI;
using TMPro;

namespace Assets.Scripts.UIScipts {
    class ToggleSetting : MonoBehaviour {

        [SerializeField]
        private Toggle toggle;
        [SerializeField]
        private TextMeshProUGUI settingName;
        [SerializeField]
        private TextMeshProUGUI helperText;
        [SerializeField]
        private SettingHelper HelperButton;

        private BoolSetting setting;
        
        public void SetSettingInfo(BoolSetting _setting) {
            
            if(HelperButton != null) {
                HelperButton.WikiLink = _setting.WikiLink;
            }

            if(settingName != null) {
                settingName.text = _setting.Name;
            }

            if(helperText != null) {
                helperText.text = "Info : \n" + _setting.HelperText;
                helperText.transform.parent.parent.gameObject.SetActive(false);
            }

            if(toggle != null) {


                UpdateValue(_setting.Value);

                toggle.onValueChanged.AddListener(ChangeValue);
            }

            setting = _setting;
        }
        

        public void ChangeValue(bool value) {
            setting.SetValue(value);
        }

        public void UpdateValue(bool value) {
            toggle.isOn = value;
        }

    }
}
