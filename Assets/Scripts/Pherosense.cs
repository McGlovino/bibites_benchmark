﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pherosense : MonoBehaviour {
    public LayerMask pheroMask;
    public Collider2D[] elementsInSenseRadius;
    float pheroDist;
    public float PheroSum1;
    public float PheroSum2;
    public float PheroSum3;
    public float angleToPhero1;
    public float angleToPhero2;
    public float angleToPhero3;
    //sensing;

    public bool usePheroSense = false;
    private geneCode gene;
    private NEATBrain brain;
    private pheromones pheros;
    private float prevDistR;
    private float prevDistG;
    private float prevDistB;

    public void startPherosensing() {
        if(!usePheroSense) {
            usePheroSense = true;
            StartCoroutine(PheroWithDelay(0.5f));
        }
    }

    IEnumerator PheroWithDelay(float delay) {
        gene = GetComponent<geneCode>();

        while(!gene.isReady) {
            //yield return new WaitForSeconds(delay);
            for (int i = 0; i < (int)(delay * 10); i++)
                yield return new WaitForFixedUpdate();
        }
        brain = GetComponent<NEATBrain>();
        usePheroSense = false;
        elementsInSenseRadius = new Collider2D[128];
        pheroDist = 1f + Mathf.Pow(10f, 1 + 0.995635194598f * (1f + (gene.genes[(int)geneCode.BibiteGenes.PheroSense] - 1f) / 10f));
        while(true) {
            //yield return new WaitForSeconds(delay);
            for (int i = 0; i < (int)(delay * 10); i++)
                yield return new WaitForFixedUpdate();
            PherosenseAround();
        }
    }

    void PherosenseAround() {
        PheroSum1 = 0f;
        PheroSum2 = 0f;
        PheroSum3 = 0f;

        angleToPhero1 = 0f;
        angleToPhero2 = 0f;
        angleToPhero3 = 0f;

        prevDistR = 0f;
        prevDistG = 0f;
        prevDistB = 0f;

        Physics2D.OverlapCircleNonAlloc(transform.position, pheroDist, elementsInSenseRadius, pheroMask, 0);

        for(int i = 0; i < elementsInSenseRadius.Length; i++) {
            if(brain.isReady && elementsInSenseRadius[i] != null) {
                GameObject lol = elementsInSenseRadius[i].gameObject;
                pheros = lol.GetComponent<pheromones>();
                Vector3 trgt = elementsInSenseRadius[i].gameObject.transform.position - transform.position;
                float pheroStr = 1f / (1f + Mathf.Pow(trgt.magnitude, 1f / (1f + (gene.genes[(int)geneCode.BibiteGenes.PheroSense] - 1f) / 10f))); //TODO:possible divide by zero

                float angle2Phero = Vector2.SignedAngle(transform.up, trgt);

                if(pheros.Rstrength > 0) {
                    PheroSum1 += pheroStr;
                    if(trgt.magnitude > prevDistR) {
                        prevDistR = trgt.magnitude;
                        angleToPhero1 = angle2Phero;
                    }
                }
                if(pheros.Gstrength > 0) {
                    PheroSum2 += pheroStr;
                    if(trgt.magnitude > prevDistG) {
                        prevDistG = trgt.magnitude;
                        angleToPhero2 = angle2Phero;
                    }
                }
                if(pheros.Bstrength > 0) {
                    PheroSum3 += pheroStr;
                    if(trgt.magnitude > prevDistB) {
                        prevDistB = trgt.magnitude;
                        angleToPhero3 = angle2Phero;
                    }
                }
            }
            else
                break;
        }
    }
}
