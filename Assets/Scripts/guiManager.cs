﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Random = UnityEngine.Random;

using Assets.Scripts.SettingScripts;

public class guiManager : MonoBehaviour {

    private int[] scaleFactors = { 60, 85, 100, 140, 180 };
    private int scaleIndex = 3;
    private GameObject target;
    private Text timeText;
    private Text nText;
    private Time_keeper timeKeep;

    private geneCode gene;
    private NEATBrain brain;
    private BibiteBody body;
    private BibiteGrowth growth;
    private player_control control;
    private eggHatch hatching;

    [Header("GameObjects")]
    public GameObject UICanvas;



    [Header("Stats Panel")]
    public GameObject noneText;
    public GameObject StatsPanel;
    public GameObject Laybutton;
    public Text statsText;

    [Header("Genes Panel")]
    public GameObject GenesPanel;
    public Text geneText;

    [Header("Brain Panel")]
    public GameObject BrainPanel;
    public GameObject NeuronHolder;
    public GameObject SynapseHolder;
    public GameObject UINeuronPref;
    public GameObject UISynapsPref;

    [Header("Settings")]
    public GameObject SettingsPanel;
    public Toggle AutosaveToggle;
    public InputField AutosaveFiles;
    public Dropdown UIScaleDropdown;
    public Toggle FullscreenToggle;

    [Header("UI Elements")]
    [SerializeField]
    private statusBars barsControler;

    private List<GameObject> UINeurons;
    private List<int> UINeurons_IDX;
    private List<GameObject> UISynapses;
    private List<int> UISynapses_IDX;

    private CanvasScaler scaler;

    private int curMenu;

    // Use this for initialization
    void Start() {

        target = GameObject.Find("timeText");
        timeText = target.GetComponent<Text>();

        target = GameObject.Find("nText");
        nText = target.GetComponent<Text>();

        //bars = GameObject.Find("statusbars").GetComponent<statusBars>();

        target = GameObject.Find("Control");
        timeKeep = target.GetComponent<Time_keeper>();
        target = null;

        FillStatsPanel();
        FillGenesPanel();
        FillBrainPanel();

        noneText.SetActive(true);
        StatsPanel.SetActive(false);
        GenesPanel.SetActive(false);
        BrainPanel.SetActive(false);

        Screen.fullScreen = PersistSettings.Fullscreen;
        FullscreenToggle.isOn = PersistSettings.Fullscreen;

        scaler = GameObject.Find("UICanvas").GetComponent<CanvasScaler>();
        UpdateUIScale();
    }

    public void SetUIScale(int scaleIdx) {
        scaleIndex = scaleIdx;
        PersistSettings.UIScaleFactor = scaleFactors[scaleIndex];
        UpdateUIScale();
    }

    public void SetFullscreen(bool fullscreen) {
        PersistSettings.Fullscreen = fullscreen;
        Screen.fullScreen = fullscreen;
        if(fullscreen) {
            Screen.fullScreenMode = FullScreenMode.ExclusiveFullScreen;
        }
        else {
            Screen.fullScreenMode = FullScreenMode.Windowed;
        }
    }

    private void UpdateUIScale() {
        float scale = PersistSettings.UIScaleFactor / 100f;

        scaler.scaleFactor = scale;
    }

    // Update is called once per frame
    void Update() {
        updateTime();

        
        if(Input.GetKeyDown(KeyCode.Escape)) {
            ToggleSettings();
        }
    }

    //New Target Bibite
    public void targetThat(GameObject _target) {
        
        if(_target == null) {
            clearTarget();
            return;
        }


        gene = _target.GetComponent<geneCode>();
        brain = _target.GetComponent<NEATBrain>();

        barsControler.targetThat(_target);

        if(_target.CompareTag("egg")) {


            Laybutton.SetActive(false);

            hatching = _target.GetComponent<eggHatch>();

        }else if(_target.CompareTag("bibite")) {

            Laybutton.SetActive(true);

            body = _target.GetComponent<BibiteBody>();
            growth = _target.GetComponent<BibiteGrowth>();
            control = _target.GetComponent<player_control>();


        }

        target = _target;



        if(UINeurons != null)
            foreach(GameObject Go in UINeurons)
                Destroy(Go);

        if(UISynapses != null)
            foreach(GameObject Go in UISynapses)
                Destroy(Go);

        BrainPanel.GetComponent<RectTransform>().sizeDelta = new Vector2(400, 100);


        FillStatsPanel();
        FillGenesPanel();
        FillBrainPanel();

        showPreviousPanel();

        StartCoroutine("UpdateRealTimeUI", 0.25f);

    }

    public void retarget() {
        targetThat(target);
    }
    //Clear Target Bibite
    public void clearTarget() {
        CloseAllPanels();
        noneText.SetActive(true);

        if(UINeurons != null)
            foreach(GameObject Go in UINeurons)
                Destroy(Go);

        if(UISynapses != null)
            foreach(GameObject Go in UISynapses)
                Destroy(Go);

        UINeurons_IDX = new List<int>();
        UISynapses_IDX = new List<int>();

        target = null;
        gene = null;
        brain = null;
        body = null;
        control = null;
    }

    
    /* 
        Show Panels Functions
    */
    public void showStatsPanel() {
        if(target != null) {
            CloseAllPanels();
            StatsPanel.SetActive(true);
        }
        curMenu = 1;
    }

    public void showGenesPanel() {
        if(target != null) {
            CloseAllPanels();
            GenesPanel.SetActive(true);
        }
        curMenu = 2;
    }

    public void showBrainPanel() {
        if(target != null) {
            CloseAllPanels();
            BrainPanel.SetActive(true);
        }
        curMenu = 3;
    }

    public void showNoneText() {
        CloseAllPanels();
        curMenu = 0;
        noneText.SetActive(true);
    }

    public void showPreviousPanel() {

        if(target != null) {
            switch(curMenu) {
                case 1:
                    showStatsPanel();
                    break;
                case 2:
                    showGenesPanel();
                    break;
                case 3:
                    showBrainPanel();
                    break;
                default:
                    showStatsPanel();
                    curMenu = 1;
                    break;
            }
        }
        else {
            showNoneText();
        }
    }

    public void CloseAllPanels() {
        noneText.SetActive(false);
        StatsPanel.SetActive(false);
        GenesPanel.SetActive(false);
        BrainPanel.SetActive(false);
    }


    /* 
        Fill Panel Functions
    */
    public void FillStatsPanel() {
        string s = "";
        if(target != null) {
            if(target.CompareTag("bibite")) {
                s = $"{"Stats",11}\t:\thealth({(int)body.health}/{(int)body.maxHealth}) E({(int)body.energy})\n" +
                    $"{"timeAlive",11}\t:\t{(int)control.clk.timeAlive}\n" +
                    $"{"maturity",11}\t:\t{growth.maturity}\n" +
                    $"{"speed",11}\t:\t{control.signedVelocity}\n" +
                    $"{"metabolism",11}\t:\t{body.metabolism}\n";
            }
            else if(target.CompareTag("egg")) {
                s = $"{"Stats",11}\t:\tProgress({(int)(hatching.hatchProgress() * hatching.initialBodyEnergy)}/{(int)hatching.initialBodyEnergy}) Energy({(int)(hatching.energy - hatching.hatchProgress() * hatching.initialBodyEnergy)})\n" +
                    $"{"timeAlive",11}\t:\t{(Time.time - hatching.startTime)}\n" +
                    $"{"growth",11}\t:\t{(float)Mathf.Round(hatching.hatchProgress() * 1000) / 10}%\n" +
                    $"{"speed",11}\t:\t0\n" +
                    $"{"metabolism",11}\t:\t0\n";
            }
            s += $"{"Generation",11}\t:\t{gene.generation}\n" +
                $"{"Brain",11}\t:\tN:{brain.nHidden} S:{brain.nSynaps}\n";
        }
        

        statsText.text = s;
    }

    public void FillGenesPanel() {
        string s = "";
        if(target != null) {
            foreach(var geneName in System.Enum.GetValues(typeof(geneCode.BibiteGenes))) {
                s += $"{geneName.ToString(),21}\t:\t{gene.genes[(int)geneName],-10}\n";
            }
        }
        geneText.text = s;
    }

    public void FillBrainPanel() {
        //brain is not initialized yet. Nothing to draw
        if(target == null || !brain.isReady)
            return;

        //Starting positioning index on the panel
        float inputIndex = 0f;
        float outputIndex = 0f;
        float hiddenIndex = 0.5f;

        float topPadding = 75f;
        float bottomPadding = 50f;

        float verticalStep = 75f;

        float panelWidth = ((RectTransform)BrainPanel.transform).rect.width;
        float CanvasScale = UICanvas.transform.localScale.x;

        int UIcnt = 0;
        UINeurons = new List<GameObject>();
        UISynapses = new List<GameObject>();
        UINeurons_IDX = new List<int>();
        UISynapses_IDX = new List<int>();

        //Assign Layer Values to Hidden
        int noHiddenLayers = 0;
        int[] nodeLayers = new int[brain.getNNeurons()];
        List<int> layerNodeCounter = new List<int>();
        for(int i = brain.getNInputs() + brain.getNOutputs(); i < brain.getNNeurons(); i++) {
            int layersOut = brain.GetNodeDepth(brain.nodes[i], 0, new List<NEATBrain.node>());
            nodeLayers[i] = layersOut;
            while(layerNodeCounter.Count < layersOut+1)
                layerNodeCounter.Add(0);
            layerNodeCounter[layersOut]++;
            noHiddenLayers = (layersOut > noHiddenLayers ? layersOut : noHiddenLayers);
        }
        int[] layerNodeCounterAdded = new int[layerNodeCounter.Count];

        //find layer with largest amount of nodes
        int maxNodes = 0;
        for(int i = 0; i < layerNodeCounter.Count; i++)
            maxNodes = (layerNodeCounter[i] > maxNodes ? layerNodeCounter[i] : maxNodes);
        int inputActive = 0;
        for(int i = 0; i < brain.getNInputs(); i++) {
            if(brain.nodes[i].nIn > 0 || brain.nodes[i].nOut > 0)
                inputActive++;
        }
        int outputActive = 0;
        for(int i = brain.getNInputs(); i < brain.getNInputs() + brain.getNOutputs(); i++) {
            if(brain.nodes[i].nIn > 0 || brain.nodes[i].nOut > 0)
                outputActive++;
        }
        maxNodes = (inputActive > maxNodes ? inputActive : maxNodes);
        maxNodes = (outputActive > maxNodes ? outputActive : maxNodes);

        //adjust panel height and vertical step to prevent brain panel overflow
        float panelHeight = topPadding + bottomPadding + maxNodes * verticalStep;
        float maxPanelHeight = Screen.height * 0.8f / (PersistSettings.UIScaleFactor / 100f);
        if(panelHeight > maxPanelHeight) {
            verticalStep *= maxPanelHeight / panelHeight;
            panelHeight = maxPanelHeight;
        }

        float inputStartValue = 0.1f;
        float outputStartValue = 0.9f;
        //Draw Neurons
        for(int i = 0; i < brain.getNNeurons(); i++) {
            if(brain.nodes[i].nIn > 0 || brain.nodes[i].nOut > 0) {
                //Draw Input Neurons
                if(i < brain.getNInputs()) {
                    
                    UINeurons.Add(Instantiate(UINeuronPref, BrainPanel.transform.position 
                        + new Vector3(inputStartValue * panelWidth * CanvasScale
                        , -CanvasScale * ((verticalStep * inputIndex) + (verticalStep * (maxNodes - inputActive) / 2f) + topPadding), 0)
                        , Quaternion.identity, NeuronHolder.transform));
                    UINeurons_IDX.Add(i);
                    inputIndex++;
                }
                //Draw Output Neurons
                else if(i < brain.getNInputs() + brain.getNOutputs()) {
                    UINeurons.Add(Instantiate(UINeuronPref, BrainPanel.transform.position 
                        + new Vector3(outputStartValue * panelWidth * CanvasScale
                        , -CanvasScale * ((verticalStep * outputIndex) + (verticalStep * (maxNodes - outputActive) / 2f) + topPadding), 0)
                        , Quaternion.identity, NeuronHolder.transform));
                    UINeurons_IDX.Add(i);
                    outputIndex++;
                }
                //Draw Hiden Neurons
                else {
                    UINeurons.Add(Instantiate(UINeuronPref, BrainPanel.transform.position
                        + new Vector3(((((outputStartValue - inputStartValue) / (noHiddenLayers + 1)) * nodeLayers[i]) + inputStartValue) * panelWidth * CanvasScale
                        , -CanvasScale * ((verticalStep * layerNodeCounterAdded[nodeLayers[i]]) + (verticalStep * ((maxNodes - layerNodeCounter[nodeLayers[i]]) / 2f)) + topPadding), 0)
                        , Quaternion.identity, NeuronHolder.transform));
                    UINeurons_IDX.Add(i);

                    layerNodeCounterAdded[nodeLayers[i]]++;

                    hiddenIndex++;
                }

                UINeurons[UIcnt].GetComponent<UINeuron>().index = i;
                UINeurons[UIcnt].GetComponent<UINeuron>().setDesc(brain.getNeuronDesc(i));
                UINeurons[UIcnt].GetComponent<UINeuron>().brain = brain;
                UINeurons[UIcnt].GetComponent<UINeuron>().gM = this;
                UIcnt++;
            }
        }

        BrainPanel.GetComponent<RectTransform>().sizeDelta = new Vector2(panelWidth, panelHeight);

        //Draw Synapses
        for(int i = 0; i < brain.nSynaps; i++) {
            GameObject newSynaps = Instantiate(UISynapsPref, BrainPanel.transform.position, Quaternion.identity, SynapseHolder.transform);
            UISynaps newSynapseScript = newSynaps.GetComponent<UISynaps>();
            newSynapseScript.index = i;
            newSynapseScript.brain = brain;
            newSynapseScript.gM = this;
            UISynapses.Add(newSynaps);
            UISynapses_IDX.Add(i);
            RectTransform rt = UISynapses[i].GetComponent<RectTransform>();

            int j = UINeurons.FindIndex(L => L.GetComponent<UINeuron>().index == brain.synapses[i].nodeOut);
            RectTransform rt1 = UINeurons[j].GetComponent<RectTransform>();

            int k = UINeurons.FindIndex(L => L.GetComponent<UINeuron>().index == brain.synapses[i].nodeIn);
            RectTransform rt2 = UINeurons[k].GetComponent<RectTransform>();

            rt.anchoredPosition = rt2.anchoredPosition;
            rt.sizeDelta = new Vector2((rt1.anchoredPosition - rt2.anchoredPosition).magnitude / 20, UISynaps.thickness);
            rt.eulerAngles = new Vector3(0, 0, Mathf.Atan2((rt1.anchoredPosition - rt2.anchoredPosition).y, (rt1.anchoredPosition - rt2.anchoredPosition).x) * Mathf.Rad2Deg);

            rt.SetAsFirstSibling();

            UISynapses[i].GetComponent<UISynaps>().setValue(brain.synapses[i].weight, brain.synapses[i].en);
        }
    }

    public void hightlightSubNetwork(List<int> hightlightedSynapses, List<int> highlightedNodes) {
        foreach(int i in hightlightedSynapses) {
            if(UISynapses_IDX.IndexOf(i) >= 0) {
                UISynapses[UISynapses_IDX.IndexOf(i)].transform.SetParent(GameObject.Find("HighlightedSynapses").transform);
                UISynapses[UISynapses_IDX.IndexOf(i)].GetComponent<RectTransform>().sizeDelta
                    = new Vector2(UISynapses[UISynapses_IDX.IndexOf(i)].GetComponent<RectTransform>().sizeDelta.x, UISynaps.highlightedThickness);
            }
            else {
                Debug.LogError("Synapse not found: thought this was fixed, contact Paolo if experinced this error");
                Debug.LogError("i: " + i + "\n" 
                    + "UINeurons_IDX.Count: " + UINeurons_IDX.Count + "\n" 
                    + "brain.nSynaps: " + brain.nSynaps);
                brain.SetSynapseIndex();
            }
        }
        foreach(int i in highlightedNodes) {
            if(UINeurons_IDX.IndexOf(i) >= 0) {
                UINeurons[UINeurons_IDX.IndexOf(i)].transform.SetParent(GameObject.Find("HighlightedNeurons").transform);
                UINeurons[UINeurons_IDX.IndexOf(i)].GetComponent<RectTransform>().sizeDelta
                    = new Vector2(UINeuron.highlightedThickness, UINeuron.highlightedThickness);
            }
            else
                Debug.LogError("Neuron not found: thought this was fixed, contact Paolo if experinced this error");
        }
    }
    public void unHightlightSubNetwork(List<int> hightlightedSynapses, List<int> highlightedNodes) {
        foreach(int i in hightlightedSynapses) {
            if(UISynapses_IDX.IndexOf(i) >= 0) {
                UISynapses[UISynapses_IDX.IndexOf(i)].transform.SetParent(GameObject.Find("SynapseHolder").transform);
                UISynapses[UISynapses_IDX.IndexOf(i)].GetComponent<RectTransform>().sizeDelta
                    = new Vector2(UISynapses[UISynapses_IDX.IndexOf(i)].GetComponent<RectTransform>().sizeDelta.x, UISynaps.thickness);
            }
            else
                Debug.LogError("Synapse not found: thought this was fixed, contact Paolo if experinced this error");
        }
        foreach(int i in highlightedNodes) {
            if(UINeurons_IDX.IndexOf(i) >= 0) {
                UINeurons[UINeurons_IDX.IndexOf(i)].transform.SetParent(GameObject.Find("NeuronHolder").transform);
                UINeurons[UINeurons_IDX.IndexOf(i)].GetComponent<RectTransform>().sizeDelta
                    = new Vector2(UINeuron.thickness, UINeuron.thickness);
            }
            else
                Debug.LogError("Neuron not found: thought this was fixed, contact Paolo if experinced this error");
        }
    }

    /*
        Update Real-time UI Functions
    */
    IEnumerator UpdateRealTimeUI(float delay) {
        while(target != null) {
            if(body != null)
                updateStatsPanel();
            if(brain != null)
                updateBrainPanel();

            if(target.CompareTag("bibite")) {
                barsControler.updateHealth(body.health, body.maxHealth);
                barsControler.updateEnergy(body.energy, body.maxEnergy);
            }
            else if(target.CompareTag("egg")) {
                barsControler.updateHealth(hatching.hatchProgress() * hatching.initialBodyEnergy, hatching.initialBodyEnergy);
                barsControler.updateEnergy(hatching.getEggFreeEnergy(), hatching.energy);
            }

            
            yield return new WaitForSeconds(delay * Time.timeScale);

        }
        clearTarget();
        barsControler.targetThat(null);
    }

    private void updateStatsPanel() {
        FillStatsPanel();
    }

    private void updateBrainPanel() {
        foreach(GameObject GObj in UINeurons) {
            UINeuron neur = GObj.GetComponent<UINeuron>();
            neur.setValue(brain.nodes[neur.index].value);
        }
    }

    private void updateTime() {
        //timeText.text = "Time : " + timeKeep.hours + ":" + timeKeep.minutes + ":" + timeKeep.seconds + "  (x" + Mathf.Round(100f * Time.timeScale) / 100f + ")";
        timeText.text = $"Time :{timeKeep.hours}:{timeKeep.minutes}:{timeKeep.seconds}  (x{Mathf.Round(100f * Time.timeScale) / 100f})\nFPS : {Mathf.Round(timeKeep.FPS)}";
    }

    public void updateBiomass(int plantCount, int meatCount, int bibiteCount, int eggCount, float pelletSpawnerBM, float pelletBM, float bibiteBM, float eggBM, float plantBM, float meatBM) {
        nText.text = $"Entities:\t{(int)(bibiteCount + plantCount + eggCount + meatCount):n0} \n" +
            $"  bibites:\t{bibiteCount:n0}\n" +
            $"  eggs:\t\t{eggCount:n0}\n" +
            $"  pellets:\t{plantCount + meatCount:n0}\n" +
            $"   plants:\t{plantCount:n0}\n" +
            $"   meats:\t{meatCount:n0}\n" +
            $"\nbiomass:\t{Mathf.Round(pelletSpawnerBM + pelletBM + bibiteBM + eggBM):n0}\n" +
            $"  total:\t{Mathf.Round(pelletSpawnerBM + pelletBM + bibiteBM + eggBM):n0}\n" +
            $"  p_bank:\t{Mathf.Round(pelletSpawnerBM):n0}\n" +
            $"  pellets:\t{Mathf.Round(pelletBM):n0}\n" +
            $"    plants:\t{Mathf.Round(plantBM):n0}\n" +
            $"    meats:\t{Mathf.Round(meatBM):n0}\n" +
            $"  bibites:\t{Mathf.Round(bibiteBM):n0}\n" +
            $"  eggs:\t\t{Mathf.Round(eggBM):n0}";
    }

    /* 
        Remaining UI functions
    */
    public void ToggleSettings() {
        if(SettingsPanel.activeSelf)
            SettingsPanel.SetActive(false);
        else {
            AutosaveToggle.isOn = PersistSettings.AutoSave;
            AutosaveFiles.text = PersistSettings.AutoSaveMaxFiles.ToString();

            var factorIdx = Array.IndexOf(scaleFactors, PersistSettings.UIScaleFactor);
            if(factorIdx == -1)
                factorIdx = 3;
            UIScaleDropdown.value = factorIdx;

            SettingsPanel.SetActive(true);
        }
    }
}