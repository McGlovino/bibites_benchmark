﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeStepManager : MonoBehaviour
{
    public int frameCount, fixedFramecount;
    public float timePassed;

    public int totalFrameCount;

    float MDT = 1f;
    float TS = 10f;//5;
    float FDT = 0.01f;

    private static TimeStepManager TSM;
    public static TimeStepManager Instance
    {
        get
        {
            if (TSM == null)
                TSM = new TimeStepManager();
            return TSM;
        }
        set
        {
            TSM = value;
        }
    }

    void Start()
    {
        TimeStepManager.Instance = this;

        Time.maximumDeltaTime = MDT;
        Time.timeScale = TS;
        Time.fixedDeltaTime = FDT;
    }
    private void FixedUpdate()
    {
        //Debug.Log("Fixed");
        fixedFramecount++;
        totalFrameCount++;
    }
    void Update()
    {
        //Debug.Log("Standard");
        frameCount++;
        timePassed += Time.deltaTime;

        if(timePassed >= 10)
        {
            //Debug.Log("Fixed Framrate: " + fixedFramecount / timePassed);
            //Debug.Log("Standard Framerate: " + frameCount / timePassed);

            timePassed = 0;
            fixedFramecount = 0;
            frameCount = 0;
        }
    }

}
