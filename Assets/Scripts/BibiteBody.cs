﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

using Assets.Scripts.SettingScripts;

public class BibiteBody : MonoBehaviour
{   
    
    public GameObject meat;
    public PelletSpawner biosys;

    private Rigidbody2D rb2d;
    private BibiteGrowth growth;
    private internClock clock;
    private geneCode gene;
    private NEATBrain brain;
    private player_control control;
    private SpriteRenderer spriteRenderer;
    public Sprite wow;

    [SerializeField]
    public float size;
    [SerializeField]
    internal float rootSize;
    [SerializeField]
    public float health;
    [SerializeField]
    public float maxHealth = 0.01f;
    internal float healthRatio = 0.01f;
    [SerializeField]
    public float energy;
    [SerializeField]
    public float maxEnergy;
    [SerializeField]
    public float wasteBank = 0;
    internal float lifeRatio = 0;
    internal float attackedDmg = 0;
    internal float metabolism;

    //exit condition
    TimeStepManager TSM;
    int startTime;
    public int framesAlive;


    public float totalEnergy {
        get {
            if (gene == null)
                gene = GetComponent<geneCode>();
            return (maxHealth + health / GameSettings.Instance.healthBodyRatio.Value) * gene.getBodyEnergyRatio() + energy;
        }
    }

    [SerializeField]
    public bool dying = false;
    [SerializeField]
    public bool isWow = false;
    [SerializeField]
    public bool born = false;

    private void InitBody() {
        rb2d = GetComponent<Rigidbody2D>();
        growth = GetComponent<BibiteGrowth>();
        clock = GetComponent<internClock>();
        gene = GetComponent<geneCode>();
        brain = GetComponent<NEATBrain>();
        control = GetComponent<player_control>();
        spriteRenderer = GetComponent<SpriteRenderer>();

        rb2d.drag = GameSettings.Instance.dragCoefficient.Value;
        rb2d.angularDrag = GameSettings.Instance.dragCoefficient.Value;

        biosys = GameObject.FindGameObjectWithTag("FoodSource").GetComponent<PelletSpawner>();
    }

    public void ResumeBody() {
        InitBody();
        StartControlling();
        growth.ResumeGrowth();
        clock.ResumeClock();
    }
    
    // Start is called before the first frame update
    public void StartBody() {
        InitBody();
        if(Random.Range(0f,100f) <= 0.01f) {
            isWow = true;
        }

        size = 0f;

        float bornSize = gene.getDefaultBornSize();
        float allHealthEnergy = gene.getBodyEnergyRatio()*(1f + 1f / GameSettings.Instance.healthBodyRatio.Value)  * 100f;

        //if not enough energy, the bibite will be born smaller 
        if(energy < allHealthEnergy * bornSize) {
            bornSize = energy / allHealthEnergy;
        }

        health = 1f;
        maxHealth = 1f;

        float catchVal = setSize(bornSize);
        useEnergy(catchVal);
        
        growth.StartGrowth();
        
        if(energy > maxEnergy) {
            useEnergy(energy - maxEnergy);
        }

        TSM = TimeStepManager.Instance;
        startTime = TSM.totalFrameCount;
        EndCondition.Instance.AddBibite(this);
        ObjectHandler.Instance.AddObject(gameObject);

        StartControlling();
    }

    private void StartControlling() {
        spriteRenderer.color = gene.getBibiteColor(); 
        
        if(isWow) {
            spriteRenderer.sprite = wow;
        }
        
        metabolism = GameSettings.Instance.metabolismCost.Value * gene.genes[(int)geneCode.BibiteGenes.SpeedRatio] * gene.genes[(int)geneCode.BibiteGenes.SizeRatio];

        control.StartControling();
        born = true;
    }

    //Every cycles
    private void FixedUpdate() {
        if(!born || !(TSM.totalFrameCount > 0f) || dying)
            return;

        framesAlive = TSM.totalFrameCount - startTime;

        metabolismCycle();
        healthRatio = health/maxHealth;

        //get actions
        brain.thinker();

        attackedDmg *= Mathf.Pow(0.9f, 0.1f); //decrease activation

            
        if(health <= 0f || (float.IsNaN(health))) {
            die();
        }  
            
        heal(2*brain.outs[(int)NEATBrain.Outputs.Want2Heal] * 0.1f);

        //wastes 
        if(wasteBank > 50f) {
            biosys.biomass += wasteBank;
            wasteBank = 0;
        }

        if (!born || !(TSM.totalFrameCount > 0f) || dying)
            return;


        control.actions();
    }

    void metabolismCycle() {
        useEnergy(metabolism * 0.1f * growth.maturity);
    }

    /// <summary>
    /// Use energy, with waste(if it's used) or without waste(if it's tranfered)
    /// </summary>
    internal void useEnergy(float qtyE, bool waste = true) {
        if(energy > qtyE) {
            energy -= qtyE;
        }
        else {
            qtyE -= energy;
            energy = 0;
            Hurting(qtyE / getHealthEnergyRatio());
        }
        if(waste) {
            wasteBank += qtyE;
        }
    }

    /// <summary>
    /// Heal for an amount of energy
    /// </summary>
    /// <returns>
    /// Energy consumed by the function
    /// </returns>
    void heal(float value) {

        value = Mathf.Min(value, maxHealth - health);
        useEnergy(value * getHealthEnergyRatio(), false);
        health += value;
    }

    /// <summary>
    /// Set energy level
    /// </summary>
    public void setEnergy(float qtyE) {
        energy = qtyE;
    }

    /// <summary>
    /// Gain energy with or without waste
    /// </summary>
    /// <returns>
    /// Energy consumed by the function
    /// </returns>
    internal float gainEnergy(float qtyE, bool waste = true) {
        float diff = maxEnergy - energy; //max energy that can be gained
        
        //if enough space, gain energy
        if(diff > qtyE) {
            energy += qtyE;
            return qtyE;
        }//else, fill energy to max
        else {
            energy = maxEnergy;

            //if(waste) waste overflow
            if(waste) {
                wasteBank += qtyE - diff;
                return qtyE;
            }//else, consume only used energy
            else {
                return diff;
            }
        }
    }

    /// <summary>
    /// Set size and update related variables.
    /// </summary>
    /// <returns>
    /// Energy cost of modifications
    /// </returns>
    internal float setSize(float newSize) {
        float prevSize = size;
        float diffSize = newSize - size;
        size = newSize;
        rootSize = Mathf.Sqrt(size);
        transform.localScale = Vector3.one * rootSize;
        health = 100f * size * health / maxHealth;
        maxHealth = 100f * size;
        maxEnergy = maxHealth * GameSettings.Instance.maxEnergyToMaxHealthRatio.Value;

        rb2d.mass = GameSettings.Instance.defaultBibiteMass.Value * size;

        return diffSize * gene.getBodyEnergyRatio() * (1f+1f/GameSettings.Instance.healthBodyRatio.Value) * 10; //was 100 before paolo changed
    }
    
    public float Attacked(float dmg) {
        attackedDmg = Hurting(dmg);
        return attackedDmg;
    }

    public float Hurting(float dmg) {
        if(dmg > health)
            dmg = health;

        health -= dmg;
        return dmg;
    }

    public float getHealthEnergyRatio() {
        return gene.getBodyEnergyRatio() / GameSettings.Instance.healthBodyRatio.Value;
    }

    public void die() {
        EndCondition.Instance.RemoveBibite(this);
        ObjectHandler.Instance.RemoveObject(gameObject);
        if (!born) {
            Destroy(gameObject);
            return;
        }
        //HOW long alive for
        //Debug.Log("Alive: " + framesAlive);
        dying = true;
        //mark to delete now, so no any further problems prevent object from being removed 
        Destroy(gameObject);
        control.removeAllJoins();

        if(wasteBank > 0f)
            biosys.biomass += wasteBank;
        wasteBank = 0;

        float totalMeatEnergy = totalEnergy;
        float avrgsize = totalMeatEnergy / 3.5f;
        GameObject meato;
        pellet pelleto;
        float meatPelletSize;
        float spawnx;
        float spawny;

        GameObject pelletHolder = WorldObjectsSpawner.PelletHolder;

        while(totalMeatEnergy > 0f) {
            if(totalMeatEnergy / avrgsize < 1.5f) {
                meatPelletSize = totalMeatEnergy;
                totalMeatEnergy = 0f;
            }
            else {
                meatPelletSize = Random.Range(0.75f * avrgsize, 1.25f * avrgsize);
                totalMeatEnergy -= meatPelletSize;
            }
            spawnx = transform.position.x + Random.Range(-5f * rootSize, 5f * rootSize);
            spawny = transform.position.y + Random.Range(-5f * rootSize, 5f * rootSize);

            meato = Instantiate(meat, new Vector3(spawnx, spawny, 0), Quaternion.identity, pelletHolder.transform);

            pelleto = meato.GetComponent<pellet>();
            pelleto.initializePellet(meatPelletSize);
        }
    }


    public float getStrength() {
        return gene.genes[(int)geneCode.BibiteGenes.Strength];
    }
}
