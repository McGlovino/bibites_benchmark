﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Assets.Scripts.SettingScripts;
using Assets.Scripts.UtilityScripts;

public class GameStarter : MonoBehaviour {
    [SerializeField]
    private AutoPanner autopan;
    private PelletSpawner pelletSpawner;
    private BibiteSpawner bibiteSpawner;

    // Start is called before the first frame update
    void Start() {
        InitializeScene();  
        BuildSpawners();
        StartSpawners();
    }
        
    public void BuildSpawners() {
        var objectSpawner = GameObject.FindObjectOfType<WorldObjectsSpawner>();

        var eggSpawner = objectSpawner.generateNewBibiteSpawner();
        var pelletSpawner = objectSpawner.generateNewPelletSpawner();

        this.pelletSpawner = pelletSpawner.GetComponent<PelletSpawner>();
        
        this.pelletSpawner.spawnBoundaries = GameSettings.Instance.SimulationSize.Value;
        this.pelletSpawner.biomass = GameSettings.Instance.biomassDensity.Value * GameSettings.Instance.SimulationSize.Value * GameSettings.Instance.SimulationSize.Value;
        this.pelletSpawner.avgSize = GameSettings.Instance.pelletSize.Value;
        this.pelletSpawner.spawnTime = GameSettings.Instance.pelletSize.Value / (GameSettings.Instance.pelletGrowth.Value * GameSettings.Instance.SimulationSize.Value * GameSettings.Instance.SimulationSize.Value);
        
        bibiteSpawner = eggSpawner.GetComponent<BibiteSpawner>();
    }

    public void StartSpawners() {
        pelletSpawner.StartSpawner();
        bibiteSpawner.StartSpawner();
        autopan.StartAutoPanner(pelletSpawner);
    }

    public void ResumeSpawners() {
        pelletSpawner.StartSpawner();
        bibiteSpawner.StartSpawner();
    }

    public void InitializeScene() {
        Camera.main.backgroundColor = Color.white * GameSettings.Instance.backgroundShade.Value;
    }

}
