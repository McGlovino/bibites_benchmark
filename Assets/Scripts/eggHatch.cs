﻿using UnityEngine;
using System.Collections;

public class eggHatch : MonoBehaviour {

    private geneCode eggGene;
    private NEATBrain eggBrain;
    private geneCode newBornGene;
    private NEATBrain newBornBrain;
    private BibiteBody newBornBody;
    private GameObject Hatched;
    
    public GameObject toHatch;
    public GameObject meat;
    
    public float energy;
    public float eggEnergy;
    public float initialBodyEnergy;
    public int startTime;
    public int hatchTime;

    private SpriteRenderer spriteRenderer;
    private player_control parent1;
    private player_control parent2;

    // Use this for initialization
    void InitEgg() {
        eggGene = GetComponent<geneCode>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        eggBrain = GetComponent<NEATBrain>();
    }

    public void StartHatch() {
        InitEgg();
        StartCoroutine(waitEggReady());
    }

    public void ResumeHatch() {
        InitEgg();
        spriteRenderer.color = eggGene.getBibiteColor();
        int remainTime = hatchTime - (TimeStepManager.Instance.totalFrameCount - startTime);
        
        StartCoroutine(hatch(remainTime));
    }

    void getParent1(player_control papa) {
        parent1 = papa;
    }

    void getParent2(player_control papa, player_control mama) {
        parent1 = papa;
        parent2 = mama;
    }

    //Measure the amount of energy free for development
    public float getEggFreeEnergy() {
        float e = energy - hatchProgress() * (initialBodyEnergy + eggBrain.getBrainCost());

        if(e <= 0)
            abort();
        return e;
    }

    IEnumerator waitEggReady() {
        bool check = false;
        while(!check) {
            check = eggGene.geneTransfered && eggBrain.isReady;
            if(!check)
                yield return new WaitForFixedUpdate();
        }

        hatchTime = (int)(eggGene.genes[(int)geneCode.BibiteGenes.HatchTime]*10);
        
        if(hatchTime <= 0) {
            abort();
        }
        else {
            initialBodyEnergy = eggGene.getBirthBodyEnergy();

            spriteRenderer.color = eggGene.getBibiteColor();
            transform.localScale = Vector3.one*Mathf.Sqrt(eggGene.getDefaultBornSize());
            startTime = TimeStepManager.Instance.totalFrameCount;
            StartCoroutine("hatch", hatchTime);
        }
    }

    IEnumerator hatch(float delay) {
        while(startTime + delay > TimeStepManager.Instance.totalFrameCount) {
            //yield return new WaitForSeconds(0.1f);
            yield return new WaitForFixedUpdate();
            getEggFreeEnergy();
        }

        Hatched = Instantiate(toHatch, transform.position, Quaternion.identity, WorldObjectsSpawner.BibiteHolder.transform) as GameObject;
        Hatched.transform.Rotate(0, 0, Random.Range(1, 360));

        newBornGene = Hatched.GetComponent<geneCode>();
        newBornBrain = Hatched.GetComponent<NEATBrain>();
        newBornBody = Hatched.GetComponent<BibiteBody>();

        newBornGene.copyGene(eggGene);
        newBornGene.mutate();
        newBornGene.generation = eggGene.generation;

        newBornBrain.copyBrain(eggBrain, false);

        float brainEnergy = newBornBrain.getBrainCost();
        if(brainEnergy + initialBodyEnergy < energy) {
            newBornBody.setEnergy(energy - brainEnergy);
        }
        else {
            abort();
            yield break;
        }
        
        newBornBody.StartBody();
        
        energy = 0;
        Destroy(gameObject);
    }

    public void abort() {
        if(newBornBody != null)
            newBornBody.die();

        float totalMeatEnergy = energy;
        float avrgsize = totalMeatEnergy / Random.Range(1f, 3.5f);
        GameObject meato;
        pellet pelleto;
        float meatPelletSize;
        float spawnx;
        float spawny;

        GameObject pelletHolder = WorldObjectsSpawner.PelletHolder;
        float rootSize = transform.localScale.x;

        while(totalMeatEnergy > 0f) {
            if(totalMeatEnergy / avrgsize < 1.5f) {
                meatPelletSize = totalMeatEnergy;
                totalMeatEnergy = 0f;
            }
            else {
                meatPelletSize = Random.Range(0.75f * avrgsize, 1.25f * avrgsize);
                totalMeatEnergy -= meatPelletSize;
            }
            spawnx = transform.position.x + Random.Range(-2f * rootSize, 2f * rootSize);
            spawny = transform.position.y + Random.Range(-2f * rootSize, 2f * rootSize);

            meato = Instantiate(meat, new Vector3(spawnx, spawny, 0), Quaternion.identity, pelletHolder.transform);

            pelleto = meato.GetComponent<pellet>();
            pelleto.initializePellet(meatPelletSize);
        }
        Destroy(gameObject);
    }

    public float hatchProgress() {
        return (TimeStepManager.Instance.totalFrameCount - startTime)/hatchTime;
    }
}