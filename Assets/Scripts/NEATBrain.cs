﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

using Assets.Scripts.SettingScripts;
using RandomExtansion;

public class NEATBrain : MonoBehaviour {
    
    public int maxInov;
    public int nInputs;
    public int nOutputs;
    public int nHidden;

    public int nActiveInputs;
    public int nActiveOutput;
   
    public int nNodes;
    public int nSynaps;

    public float brainWeight;

    public NEATBrain tempBrain;
    public NEATBrain tempBrain2;
    [SerializeField]
    public bool isReady;
    [SerializeField]
    public bool parent;

    
    public List<node> nodese;
    public List<synaps> synapse;
    
    [SerializeField]
    public node[] nodes;
    [SerializeField]
    public synaps[] synapses;

    //Debugs tableau
    public float[] outs;
    public float[] ins;
    public float[] syns;
    
    //
    private bool[] changeN;
    private bool[] changeS;
    private List<int> disabledSynapses = new List<int>();
    public int tempInt;

    private player_control control;
    private BibiteBody body;
    private BibiteGrowth growth;
    private geneCode gene;
    private FieldOfView fow;
    private Pherosense phero;
    private internClock clk;
    
    public List<int> braintargets = new List<int>();
    public List<List<int>> brainlinks = new List<List<int>>();

    public enum Inputs {
        Constant,
        EnergyRatio, Maturity, LifeRatio, Speed,
        AttackedDamage,
        // All Vision related stuff goes here. Later code is dependent on BibiteConcentrationWeight being the first and ClosestBibiteB being the last
        // This should be changed - it's quite fragile.
        BibiteConcentrationWeight, BibiteConcentrationAngle, NVisibleBibites,
        PelletConcentrationWeight, PelletConcentrationAngle, NVisiblePellets,
        MeatConcentrationWeight, MeatConcentrationAngle, NVisibleMeat,
        ClosestBibiteR, ClosestBibiteG, ClosestBibiteB,

        // Time code here.
        Tic, Minute, TimeAlive,
        // All Phero Inputs go here. Later code is dependent on PheroSense1 being the first and Phero3Angle being the last
        PheroSense1, PheroSense2, PheroSense3,
        Phero1Angle, Phero2Angle, Phero3Angle
    };
    private Inputs[] possibleFoodSourceAngles = new Inputs[] { Inputs.PelletConcentrationAngle, Inputs.BibiteConcentrationAngle, Inputs.MeatConcentrationAngle };

    // You can use this to directly access output neurons, like outs[(int)Outputs.Want2Lay]
    public enum Outputs {
        Accelerate,
        Rotate,
        Herding,
        Want2Lay, Want2Eat, Want2Sex,
        Grab,
        ClkReset,
        PhereOut1, PhereOut2, PhereOut3,
        Want2Grow, Want2Heal, Want2Attack
    }

    /// <summary>
    /// List of the different types of Nodes used in the brain.
    /// </summary>
    public enum NodeType {
        /// <summary>
        /// Only used for the Input neurons that provide data into the neural net
        /// </summary>
        Input,
        Sigmoid,
        Linear,
        TanH,
        Sine,
        ReLU,
        Gaussian,
        /// <summary>
        /// Uses memory of prior outputs. If the input > 1, then outputs 1. If < 0, outputs 0. If between 0-1, outputs whatever it did last frame.
        /// </summary>
        Latch,
        /// <summary>
        /// Uses memory of prior inputs. Outputs the difference between the current input and the input from the last frame.
        /// </summary>
        Differential
    }

    private void BaseInit() {
        body = GetComponent<BibiteBody>();
        growth = GetComponent<BibiteGrowth>();
        gene = GetComponent<geneCode>();
        control = GetComponent<player_control>();
        fow = GetComponent<FieldOfView>();
        clk = GetComponent<internClock>();
        phero = GetComponent<Pherosense>();

        nInputs = System.Enum.GetValues(typeof(Inputs)).Length;
        nOutputs = System.Enum.GetValues(typeof(Outputs)).Length;
    }

    public void ResumeBrain() {
        BaseInit();

        nHidden = nodes.Length - nInputs - nOutputs;
        nSynaps = synapses.Length;

        ins = new float[nInputs];
        outs = new float[nOutputs];
        syns = new float[nSynaps];
        
        computeActiveNeurons();
        computeProcessingStructure();
        StartUsedSystems();

        isReady = true;
    }
    
    public void StartBrain() {
        BaseInit();
        isReady = false;
        nodese = new List<node>();
        synapse = new List<synaps>();
    }

    public void getSenses() {
        nodes[(int)Inputs.Constant].value = 1;
        nodes[(int)Inputs.EnergyRatio].value = body.energy/ body.maxEnergy;
        nodes[(int)Inputs.Maturity].value = growth.maturity;
        nodes[(int)Inputs.LifeRatio].value = body.healthRatio;
        nodes[(int)Inputs.Speed].value = (float)(control.signedVelocity/20f);
        
        float dmg = (float)body.attackedDmg / body.health; 
        nodes[(int)Inputs.AttackedDamage].value = float.IsNaN(dmg) ? 0 : dmg; // Could be NaN if bibite has died.

        nodes[(int)Inputs.BibiteConcentrationWeight].value = normalizeConcentrationWeight(fow.bibiteConcentrationWeight);
        nodes[(int)Inputs.BibiteConcentrationAngle].value = normalizeAngle(fow.bibiteConcentrationAngle);
        nodes[(int)Inputs.PelletConcentrationWeight].value = normalizeConcentrationWeight(fow.pelletConcentrationWeight);
        nodes[(int)Inputs.PelletConcentrationAngle].value = normalizeAngle(fow.pelletConcentrationAngle);
        nodes[(int)Inputs.MeatConcentrationWeight].value = normalizeConcentrationWeight(fow.meatConcentrationWeight);
        nodes[(int)Inputs.MeatConcentrationAngle].value = normalizeAngle(fow.meatConcentrationAngle);

        nodes[(int)Inputs.NVisibleBibites].value = fow.Nbibite / 4f;
        nodes[(int)Inputs.NVisiblePellets].value = fow.Npellet / 4f;
        nodes[(int)Inputs.NVisibleMeat].value = fow.NMeat / 4f;

        nodes[(int)Inputs.ClosestBibiteR].value = fow.targetR;
        nodes[(int)Inputs.ClosestBibiteG].value = fow.targetG;
        nodes[(int)Inputs.ClosestBibiteB].value = fow.targetB;

        nodes[(int)Inputs.Tic].value = clk.tic;
        nodes[(int)Inputs.Minute].value = clk.chronoTime / 20f;
        nodes[(int)Inputs.TimeAlive].value = clk.timeAlive / 20f;
        nodes[(int)Inputs.PheroSense1].value = phero.PheroSum1;
        nodes[(int)Inputs.PheroSense2].value = phero.PheroSum2;
        nodes[(int)Inputs.PheroSense3].value = phero.PheroSum3;
        nodes[(int)Inputs.Phero1Angle].value = normalizeAngle(phero.angleToPhero1);
        nodes[(int)Inputs.Phero2Angle].value = normalizeAngle(phero.angleToPhero2);
        nodes[(int)Inputs.Phero3Angle].value = normalizeAngle(phero.angleToPhero3);

        bool output = false;
        for(int i = 0; i < nInputs; i++) {
            ins[i] = nodes[i].value;
#if DEBUG
            // NOTE: To output the histograms to the console and log file, attach with a debugger, break on the following line, 
            //       change the output variable to true, and then continue. That will output all histograms one time.
            if(i > 0) { // Skip the Constant node.
                BinCounter curLogger = body.biosys.InputLoggers[(int)i];
                if(nodes[i].value != 0f && curLogger != null) { // Not logging exact zeros now because so many observations are typically 0
                    curLogger.Log(nodes[i].value);
                }
                if(output && curLogger != null) { 
                    UnityEngine.Debug.Log(System.Enum.GetName(typeof(Inputs), i) + "\n" + body.biosys.InputLoggers[(int)i].GetHistogram());
                }
            }
#endif
         }
    }

    // Normalizes with this curve:
    // https://www.desmos.com/calculator/aqryf1oahe
    private float normalizeAngle(float angle) {
        // Our angles go from -180 to +180.
        float angleInRadians = angle * (Mathf.PI / 180);

        // We want the sin value to go from 0 at 0 to 1 at 180 (rather than 90), so flatten the output 
        float flattened = Mathf.Sin(.5f * angleInRadians);

        // Spread out the result so typical results are between -1 to +1, with max of -3, +3 for better neuron response.
        return 3 * flattened;
    }

    private float normalizeConcentrationWeight(float weight) {
        // Given common weights, returns values from 0 (when weight is 0 and nothing is visible) to 2
        return Mathf.Log10(1 + weight);
    }

    public void thinker() {
        getSenses();

        float currentInput = 0f;

        for(int i = 0; i < braintargets.Count; i++) {
            currentInput = 0f;
            for(int j = 0; j < brainlinks[i].Count; j++)
                currentInput += synapses[brainlinks[i][j]].weight * nodes[synapses[brainlinks[i][j]].nodeIn].value;
            ExecuteNeuron(ref nodes[braintargets[i]], currentInput);
        }

        for(int i = 0; i < nOutputs; i++)
            outs[i] = nodes[i + nInputs].value;
    }

    public void defaultParams() {
        nHidden = 0;
        nSynaps = nOutputs;
    }

    public void copyBrain(NEATBrain papa, bool mutate, bool isVirginBirth = false) {
        StartBrain();

        nHidden = papa.nHidden;
        nSynaps = papa.nSynaps;
        parent = true;
        int nMutations = 0;

        int i = 0;
        while(nodese.Count < papa.nHidden + nOutputs + nInputs) {
            copyNode(papa.nodes[i]);
            i++;
        }

        i = 0;
        while(synapse != null && synapse.Count < papa.nSynaps) {
            copySynaps(papa.synapses[i]);
            i++;
        }

        if(isVirginBirth) {
            // Start by seeding the organism with at least a chance of interesting connections.
            int bias = GameSettings.Instance.helpfulMutations.Value;
            int numFoodSources = possibleFoodSourceAngles.Length;

            if(bias > 0)
                AddSynapse(Inputs.Constant, Outputs.Accelerate);
            if(bias > 1)
                AddSynapse(possibleFoodSourceAngles[Random.Range(0, numFoodSources)], Outputs.Rotate);
            if(bias > 2) {
                float r = Random.value;

                if(r < 0.33f) {
                    AddSynapse(null, Outputs.PhereOut1);
                    AddSynapse(Inputs.PheroSense1, null);
                }else if(r < 0.66f) {
                    AddSynapse(null, Outputs.PhereOut2);
                    AddSynapse(Inputs.PheroSense2, null);
                }
                else {
                    AddSynapse(null, Outputs.PhereOut3);
                    AddSynapse(Inputs.PheroSense3, null);
                }
            }
        }
         
        if(mutate) {
            var r = new System.Random(UnityEngine.Random.Range(int.MinValue, int.MaxValue));
            int n = r.NextPoisson(gene.genes[(int)geneCode.BibiteGenes.AverageMutationNumber]);

            for(int l = 0; l < n; l++) {

                if(r.NextDouble() < 0.75f && nSynaps > 1) {
                    int sel = r.Next(0, nSynaps);
                    ChangeSynapse(sel);
                }
                else
                    mutateBrainStructure();
            }
        }
        initBrain();
    }

    public void initBrain() {
        nodes = new node[nInputs + nOutputs + nHidden];
        synapses = new synaps[nSynaps];
        syns = new float[nSynaps];

        //Debug
        ins = new float[nInputs];
        outs = new float[nOutputs];
        syns = new float[nSynaps];        

        braintargets = new List<int>();
        brainlinks = new List<List<int>>();        

        setBaseNeuronTypes();

        if(parent) {
            //Copy all synapses
            for(int i = 0; i < nSynaps; i++) {
                synapses[i].inov = synapse[i].inov;
                synapses[i].nodeIn = synapse[i].nodeIn;
                synapses[i].nodeOut = synapse[i].nodeOut;
                synapses[i].weight = synapse[i].weight;
                synapses[i].en = synapse[i].en;
                synapses[i].index = synapse[i].index;
            }

            //Copy all nodes 
            for(int i = 0; i < nInputs + nOutputs + nHidden; i++) {
                nodes[i].inov = nodese[i].inov;
                nodes[i].type = nodese[i].type;
                nodes[i].desc = getNeuronDesc(i);
                nodes[i].nIn = nodese[i].nIn;
                nodes[i].nOut = nodese[i].nOut;
                CopyNodeSynapses(nodese[i], nodes[i]);
            }
        }

        nodese.Clear();
        if(synapse != null)
            synapse.Clear();

        //Finalize brain 
        if(nodes.Length == nInputs + nOutputs + nHidden && synapses.Length == nSynaps) {
            //give outputs their initial value 
            for(int i = nInputs; i < nInputs + nOutputs + nHidden; i++)
                ExecuteNeuron(ref nodes[i], 0f);

            //compute number of active neurons of each type
            computeActiveNeurons();
            computeProcessingStructure();
            StartUsedSystems();
            isReady = true;        
        }
        else
            body.die();
    }

    public float getBrainCost() {
        return GameSettings.Instance.neuronBirthCost.Value * nHidden + GameSettings.Instance.synapseBirthCost.Value * nSynaps;
    }

    public int getNInputs() {
        return nInputs;
    }

    public int getNOutputs() {
        return nOutputs;
    }

    public int getNHidden() {
        return nHidden;
    }

    public int getNNeurons() {
        return nInputs + nOutputs + nHidden;
    }

    private void computeActiveNeurons() {        
        nActiveInputs = 0;
        nActiveOutput = 0;

        for(int i = 0; i < nInputs + nOutputs; i++) {
            nodes[i].nIn = 0;
            nodes[i].nOut = 0;

            for(int j = 0; j < nSynaps; j++) {
                if(synapses[j].nodeIn == i)
                    nodes[i].nOut++;
                else if(synapses[j].nodeOut == i)
                    nodes[i].nIn++;
            }
            if(i < nInputs && nodes[i].nOut > 0)
                nActiveInputs++;

            if(i >= nInputs && nodes[i].nIn > 0)
                nActiveOutput++;
        }
    }

    private void computeProcessingStructure() {
        //create fast-itterable structure
        for(int i = 0; i < nSynaps; i++) {
            int index = braintargets.IndexOf(synapses[i].nodeOut);

            nodes[synapses[i].nodeOut].nIn++;
            nodes[synapses[i].nodeIn].nOut++;
            if(synapses[i].nodeOut > 0)
                SetNodeSynapsIn(synapses[i], synapses[i].nodeOut);
            if(synapses[i].nodeIn > 0)
                SetNodeSynapsOut(synapses[i], synapses[i].nodeIn);

            if(synapses[i].en) {
                if(index < 0) {
                    braintargets.Add(synapses[i].nodeOut);
                    brainlinks.Add(new List<int>());
                    brainlinks[brainlinks.Count - 1].Add(i);
                }
                else
                    brainlinks[index].Add(i);
            }
        }
    }

    private void StartUsedSystems() {
        
        //Pheromone Sensing system
        if( phero != null &&
            nodes[(int)Inputs.PheroSense1].nOut+
            nodes[(int)Inputs.PheroSense2].nOut +
            nodes[(int)Inputs.PheroSense3].nOut +
            nodes[(int)Inputs.Phero1Angle].nOut +
            nodes[(int)Inputs.Phero2Angle].nOut +
            nodes[(int)Inputs.Phero3Angle].nOut > 0) 
        {
            phero.startPherosensing();
        }

        //Vision System
        if( fow != null && 
            nodes[(int)Inputs.NVisibleBibites].nOut +
            nodes[(int)Inputs.NVisibleMeat].nOut +
            nodes[(int)Inputs.NVisiblePellets].nOut +
            nodes[(int)Inputs.BibiteConcentrationAngle].nOut +
            nodes[(int)Inputs.MeatConcentrationAngle].nOut +
            nodes[(int)Inputs.PelletConcentrationAngle].nOut +
            nodes[(int)Inputs.BibiteConcentrationWeight].nOut +
            nodes[(int)Inputs.MeatConcentrationWeight].nOut +
            nodes[(int)Inputs.PelletConcentrationWeight].nOut +
            nodes[(int)Inputs.ClosestBibiteR].nOut +
            nodes[(int)Inputs.ClosestBibiteG].nOut +
            nodes[(int)Inputs.ClosestBibiteB].nOut > 0) 
        {
            fow.startSeeing();
        }
    }

    private void copyNode(node n) {
        nodese.Add(new node(n.type, n.inov, n.desc));
        CopyNodeSynapses(n, nodese[nodese.Count-1]);
    }

    private void copySynaps(synaps syn) {
        synapse.Add(new synaps(syn.inov, syn.nodeIn, syn.nodeOut, syn.weight, syn.en, syn.index));
    }

    public void randAllSynaps() {
        for(int i = 0; i < nSynaps; i++)
            synapses[i].weight = Random.Range(-3f, 3f);
    }

    public void setBaseNeuronTypes() {

        for(int i = 0; i < nInputs; i++)
            nodes[i].type = NEATBrain.NodeType.Input;

        // Set outputs default to SIG.
        for(int i = nInputs; i < nInputs + nOutputs; i++)
            nodes[i].type = NodeType.Sigmoid;

        nodes[nInputs + (int)Outputs.Accelerate].type = NodeType.TanH;
        nodes[nInputs + (int)Outputs.Rotate].type = NodeType.TanH;
        nodes[nInputs + (int)Outputs.Herding].type = NodeType.TanH;

        //Pheromones are min 0, linear output
        nodes[nInputs + (int)Outputs.PhereOut1].type = NEATBrain.NodeType.ReLU;
        nodes[nInputs + (int)Outputs.PhereOut2].type = NEATBrain.NodeType.ReLU;
        nodes[nInputs + (int)Outputs.PhereOut3].type = NEATBrain.NodeType.ReLU;
    }

    private void mutateBrainStructure() {
        // Randomly pick which change to make from a weighted set of selections. 
        // For example, for typical neural net evolution it probably makes more sense to add a synapse far more often than a neuron type.
        // The if structure here allows you to fall through to each option successively to see if it takes. 
        float r = Random.Range(0f, 1f);

        if(r <= .55)
            AddSynapse();
        else if(r <= .70) {
            if(nSynaps > 0) { ToggleSynapseEnabled(Random.Range(1, nSynaps) - 1); }
        }
        else if(r <= .80) {
            if(nHidden > 0) { ChangeNeuron(Random.Range(nInputs + nOutputs, nInputs + nOutputs + nHidden) - 1); }
        }
        else if(r <= .90) {
            if(nSynaps > 0) { AddNeuron(); }
        }
        else if(r <= .95) {
            // Todo: Doesn't seem right yet. Fix it so it only removes previously disabled synapses, without leaving dangling hidden neurons
            //RemoveDisabledSynapse();
            RemoveSynapse(Random.Range(0, nSynaps - 1), false);
        }
        else if(r <= 1) {
            if(nHidden > 0) { RemoveNeuron(Random.Range(nInputs + nOutputs, nInputs + nOutputs + nHidden) - 1, false); }
        }
    }

    public void RemoveNeuron(int sel, bool fromRES) {
        if(sel >= nInputs + nOutputs) { //It's only possible to delete hidden neurons 
            if(nHidden > 0) {
                //create change matrices if it's the first itteration
                if(!fromRES) {
                    changeN = new bool[nHidden];
                    changeS = new bool[nSynaps];
                }

                //tag the neuron
                changeN[sel - nInputs - nOutputs] = true;

                //check every synapse
                for(int i = 0; i < nSynaps; i++) {

                    //remove every synapse that were pointing to the removed neuron
                    if((synapse[i].nodeIn == sel || synapse[i].nodeOut == sel) && !changeS[i]) 	//if either nodeIn or nodeOut was the removed neuron
                        RemoveSynapse(i, true);
                }

                if(!fromRES)
                    finRE();
            }
        }
    }

    public void RemoveSynapse(int sel, bool fromRemovedNeuron) {
        if(nSynaps > 0) {
            node tnode;

            //create change matrices if it's the first itteration
            if(!fromRemovedNeuron) {
                changeN = new bool[nHidden];
                changeS = new bool[nSynaps];
            }

            //decrement the number of connection from each connected neuron
            tnode = nodese[synapse[sel].nodeIn];
            tnode.nOut--;
            tnode.synapsOut.Remove(synapse[sel]);
            nodese[synapse[sel].nodeIn] = tnode;

            tnode = nodese[synapse[sel].nodeOut];
            tnode.nIn--;
            tnode.synapsIn.Remove(synapse[sel]);
            nodese[synapse[sel].nodeOut] = tnode;

            //tag the synapse to be removed
            changeS[sel] = true;
            disabledSynapses.Remove(sel);

            //remove all neurons hidden that are free-floting (no connexions)
            for(int i = nInputs + nOutputs; i < nInputs + nOutputs + nHidden; i++) {
                if((nodese[i].nIn < 1 || nodese[i].nOut < 1) && !changeN[i - nOutputs - nInputs])
                    RemoveNeuron(i, true);
            }

            if(!fromRemovedNeuron)
                finRE();
        }
    }

    //Finalise the removing of elements
    public void finRE() {
        //Remove tagged neurons
        for(int i = nHidden - 1; i >= 0; i--) {
            if(changeN[i]) {
                nHidden--;
                nodese.RemoveAt(i + nInputs + nOutputs);

                for(int r = 0; r < nSynaps; r++) {

                    //repoint the synapse to the right neurons 
                    if(synapse[r].nodeIn >= i + nInputs + nOutputs) {
                        synaps syns = synapse[r];
                        syns.nodeIn--;
                        synapse[r] = syns;
                    }

                    //repoint the synapse to the right neurons
                    if(synapse[r].nodeOut >= i + nInputs + nOutputs) {
                        synaps syns = synapse[r];
                        syns.nodeOut--;
                        synapse[r] = syns;
                    }
                }
            }
        }

        //remove tagged Synapses
        for(int i = nSynaps - 1; i >= 0; i--) {
            if(changeS[i]) {
                nSynaps--;
                synapse.RemoveAt(i);
            }
        }
        for(int j = 0; j < nSynaps; j++)
            synapse[j].SetIndex(j);
    }

    public void AddNeuron() {
        //ajouter un neurone à un synapse
        int sel = Random.Range(0, nSynaps - 1);  //Choose one random synapse

        //create the new neuron
        nHidden++;
        NodeType type = (NodeType)Random.Range(1, System.Enum.GetValues(typeof(NodeType)).Length);
        int inov = 0;						//inovation number
        nodese.Add(new node(type, inov, "Hidden" + (nHidden - 1))); //create

        //Move the synapse so it's tied to the new neuron
        synaps syne = synapse[sel];
        int tempOut = syne.nodeOut; 		    //keep track of the previous nodeOut
        syne.nodeOut = nodese.Count - 1;		//point to the new neuron 
        synapse[sel] = syne;

        //Create a new Synapse thats going from the new neuron to the previous nodeOut
        nSynaps++;
        synaps temp = new synaps(0, nodese.Count - 1, tempOut, 1, true, nSynaps-1);
        synapse.Add(temp);

        SetNodeSynapsIn(temp, tempOut);
        SetNodeSynapsOut(temp, nodese.Count - 1);
    }

    public void ToggleSynapseEnabled(int sel) {
        synaps tsyn;
        tsyn = synapse[sel];
        if(tsyn.en) {
            tsyn.en = false;
            disabledSynapses.Add(sel);
        }
        else {
            tsyn.en = true;
            disabledSynapses.Remove(sel);
        }
        synapse[sel] = tsyn;
    }

    private void RemoveDisabledSynapse() {
        if(disabledSynapses.Count > 0) {
            int idx = Random.Range(0, disabledSynapses.Count);
            RemoveSynapse(disabledSynapses[idx], false);
        }
    }

    public void ChangeNeuron(int sel) {
        node tnode;
        tnode = nodese[sel];
        tnode.type = (NodeType)Random.Range(1, System.Enum.GetValues(typeof(NodeType)).Length + 1);
        nodese[sel] = tnode;
    }

    public void AddSynapse(Inputs? nodeIn = null, Outputs? nodeOut = null, float? weight = null) {
        int iIdx, oIdx;
        if(nodeIn == null) {
            //chose any input or hidden neuron for nodeIn
            iIdx = Random.Range(0, nInputs + nHidden - 1);

            if(iIdx >= nInputs)
                iIdx += nOutputs;
        }
        else 
            iIdx = (int)nodeIn;

        if(nodeOut == null)
            oIdx = Random.Range(nInputs, nInputs + nOutputs + nHidden - 1);  //chose any hidden or output neuron for nodeOut
        else
            oIdx = (int)nodeOut + nInputs;

        if(weight == null)
            weight = Random.Range(-2f, 2f);

        //add one to the number of outgoing connections from nodeIn
        node nodet = nodese[iIdx];
        nodet.nOut++;
        nodese[iIdx] = nodet;


        //add one to the number of ingoing connexion to nodeOut
        nodet = nodese[oIdx];
        nodet.nIn++;
        nodese[oIdx] = nodet;

        //Create a new synapse with the generated parameters
        nSynaps++;
        synaps temp = new synaps(0, iIdx, oIdx, (float)weight, true, nSynaps-1);
        synapse.Add(temp);

        SetNodeSynapsIn(temp, oIdx);
        SetNodeSynapsOut(temp, iIdx);
    }

    public void ChangeSynapse(int sel) {
        synaps tsyn;
        tsyn = synapse[sel];
        var r = new System.Random();

        float val = r.NextGaussian(0f, gene.genes[(int)geneCode.BibiteGenes.MutationAmountSigma]);
        tsyn.weight += (0.5f + tsyn.weight) * val;

        if(1f * Random.Range(0.00f, 1.00f) < 0.05f)
            tsyn.weight = -1f * tsyn.weight;
        synapse[sel] = tsyn;
    }

    private void ExecuteNeuron(ref node n, float input) {
        float result = 0;
        switch(n.type) {
            case NodeType.Input:
                result = input;
                break;
            case NodeType.Sigmoid:
                result = 1 / (1 + Mathf.Pow(2f, -input));
                break;
            case NodeType.Linear:
                if(Mathf.Abs(input) >= 100)
                    result = Mathf.Sign(input) * 100;
                else
                    result = input;
                break;
            case NodeType.TanH:
                result = (float)System.Math.Tanh(input);
                break;
            case NodeType.Sine:
                result = Mathf.Sin(input);
                break;
            case NodeType.ReLU:
                if(input >= 100)
                    result = 100;
                else {
                    if(input > 0)
                        result = input;
                    else
                        result = 0;
                }
                break;
            case NodeType.Gaussian:
                result = 1 / (1 + Mathf.Pow(input, 2));
                break;
            case NodeType.Latch:
                if(input > 1) result = 1;
                else if(input < 0) result = 0;
                else result = n.lastOutput;
                break;
            case NodeType.Differential:
                result = (input - n.lastInput) / 0.1f;
                break;
            default:
                result = 0;
                break;
        }
        n.value = result;
        n.lastOutput = result;
        n.lastInput = input;
    }

    public int GetNodeDepth(node n, int depth, List<node> covered) {
        int max = 0;
        if(covered.Contains(n) || n.synapsIn == null)
            return depth;
        covered.Add(n);
        for(int i = 0; i < n.synapsIn.Count; i++) {
            int temp = GetNodeDepth(nodes[n.synapsIn[i].nodeIn], depth + 1, covered);
            max = (temp > max ? temp : max);
        }
        return max;
    }

    public List<int> GetConnectedNeurons(synaps toFind, List<int> olderValues, bool firstrun = true) {
        //if(firstrun) {
        //    for(int i = 0; i < synapse.Count; i++)
        //        toFind.SetIndex(i);
        //}
        
        //Exit if already done
        if(olderValues.Contains(toFind.nodeIn) && olderValues.Contains(toFind.nodeOut))
            return new List<int>();

        List<int> rv = new List<int>();

        //Get next runs
        if(!olderValues.Contains(toFind.nodeOut)) {
            rv.Add(toFind.nodeOut);
            olderValues.Add(toFind.nodeOut);
            //Debug.Log("Add: " + toFind.nodeOut);
        }
        if(nodes[toFind.nodeOut].synapsOut != null) {
            for(int i = 0; i < nodes[toFind.nodeOut].synapsOut.Count; i++)
                rv.AddRange(GetConnectedNeurons(nodes[toFind.nodeOut].synapsOut[i], olderValues, false));
        }

        if(!olderValues.Contains(toFind.nodeIn)) {
            rv.Add(toFind.nodeIn);
            olderValues.Add(toFind.nodeIn);
            //Debug.Log("Add: " + toFind.nodeIn);
        }
        if(nodes[toFind.nodeIn].synapsIn != null) {
            for(int i = 0; i < nodes[toFind.nodeIn].synapsIn.Count; i++)
                rv.AddRange(GetConnectedNeurons(nodes[toFind.nodeIn].synapsIn[i], olderValues, false));
        }

        return rv;
    }
    public List<int> GetConnectedSynapses(synaps toFind, List<int> olderValues, bool firstrun = true) {
        if(firstrun)
            SetSynapseIndex();

        //Exit if already done
        if(olderValues.Contains(toFind.index))
            return new List<int>();
        //Exit if synapse disabled
        if(!toFind.en || disabledSynapses.Contains(toFind.index))
            return new List<int>();
        //Exit if synapse number isnt within number of synaps
        if(toFind.index >= nSynaps)
            return new List<int>();

        List<int> rv = new List<int>();

        //Add synaps of this run
        rv.Add(toFind.index);
        olderValues.Add(toFind.index);

        //Get next runs
        if(nodes[toFind.nodeOut].synapsOut != null) {
            for(int i = 0; i < nodes[toFind.nodeOut].synapsOut.Count; i++)
                rv.AddRange(GetConnectedSynapses(nodes[toFind.nodeOut].synapsOut[i], olderValues, false));
        }
        if(nodes[toFind.nodeIn].synapsIn != null) {
            for(int i = 0; i < nodes[toFind.nodeIn].synapsIn.Count; i++)
                rv.AddRange(GetConnectedSynapses(nodes[toFind.nodeIn].synapsIn[i], olderValues, false));
        }
        return rv;
    }

    public void SetSynapseIndex() {
        for(int i = 0; i < synapses.Length; i++) {
            synapses[i].SetIndex(i);
            if(i >= nSynaps)
                Debug.Log("More synapses than count");
        }
    }

    public struct node {
        public NodeType type;
        public int inov;
        public int nIn;
        public int nOut;
        public string desc;
        public float value;
        public float lastInput;
        public float lastOutput;
        public List<synaps> synapsIn;
        public List<synaps> synapsOut;

        public node(NodeType _type, int _inov, string _desc) {
            type = _type;
            inov = _inov;
            desc = _desc;
            nIn = 0;
            nOut = 0;
            value = 0;
            lastInput = 0;
            lastOutput = 0;
            synapsIn = new List<synaps>();
            synapsOut = new List<synaps>();
        }
    }
    public struct synaps {
        public int inov;
        public int nodeIn;
        public int nodeOut;
        public float weight;
        public bool en;
        public int index;

        public synaps(int _inov, int _nodeI, int _nodeO, float _weight, bool _en, int _index) {
            inov = _inov;
            nodeIn = _nodeI;
            nodeOut = _nodeO;
            weight = _weight;
            en = _en;
            index = _index;
        }
        public void SetIndex(int _index) {
            index = _index;
        }
    }

    public void SetNodeSynapsIn(synaps s, int n) {
        //if(nodes != null && nodes.Length > n) {
        //    Debug.Log("sni Nodes.Length: " + nodes.Length);
        //    Debug.Log("sni s.nodeIn: " + s.nodeIn);
        //    Debug.Log("sni n: " + n);
        //    Debug.Log("sni nodes[n].synapsIn: " + nodes[n].synapsIn.Count);
        //}

        if(nodes != null && nodes.Length > n) {
            if(nodes[n].synapsIn == null)
                nodes[n].synapsIn = new List<synaps>();

            nodes[n].synapsIn.Add(s);
        }
    }
    public void SetNodeSynapsOut(synaps s, int n) {
        if(nodes != null && nodes.Length > n) {
            if(nodes[n].synapsOut == null)
                nodes[n].synapsOut = new List<synaps>();

            nodes[n].synapsOut.Add(s);
        }
    }
    public void CopyNodeSynapses(node oldNode, node newNode) {
        newNode.synapsIn = oldNode.synapsOut;
        newNode.synapsOut = oldNode.synapsOut;
    }

    public string getNeuronDesc(int i) {
        string desc = "";
        // Name of Neuron
        if(i < nInputs) { desc += System.Enum.GetName(typeof(Inputs), i); }
        else if(i < nInputs + nOutputs) { desc += System.Enum.GetName(typeof(Outputs), i - nInputs); }
        else { desc += "Hidden" + (i - nInputs - nOutputs); }

        // Type 
        desc += $" ({(nodes[i].type.ToString())})";
        return desc;
    }
}