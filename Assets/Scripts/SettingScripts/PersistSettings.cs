using UnityEngine;

namespace Assets.Scripts.SettingScripts { 

    public static class PersistSettings {
    
        public static bool AutoSave {
            get => PlayerPrefs.GetInt("AutoSave", 1) == 1;
            set => PlayerPrefs.SetInt("AutoSave", value ? 1 : 0);
        }
    
        public static int AutoSaveMaxFiles {
            get => PlayerPrefs.GetInt("AutoSaveFiles", 75);
            set => PlayerPrefs.SetInt("AutoSaveFiles", value);
        }
    
        public static bool ShowPheromones {
            get => PlayerPrefs.GetInt("ShowPheromones", 1) == 1;
            set => PlayerPrefs.SetInt("ShowPheromones", value ? 1 : 0);
        }

        public static int UIScaleFactor {
            get => PlayerPrefs.GetInt("UIScaleFactor", 100);
            set => PlayerPrefs.SetInt("UIScaleFactor", value);
        }

        public static bool Fullscreen {
            get => PlayerPrefs.GetInt("FullscreenMode", 1) == 1;
            set => PlayerPrefs.SetInt("FullscreenMode", value ? 1 : 0);
        }
    }
}
