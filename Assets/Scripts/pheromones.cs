﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;


public class pheromones : MonoBehaviour {
    [SerializeField]
    private float StartTime;

    [SerializeField]
    public float Rstrength;
    [SerializeField]
    public float Gstrength;
    [SerializeField]
    public float Bstrength;

    [SerializeField]
    public float Emission;
    [SerializeField]
    public int pheroType; // R = 1; G = 2; B = 3;

    [SerializeField]
    private float lastTime;

    private Controll contr;

    private void Start() {
        GameObject control = GameObject.Find("Control");
        contr = control.GetComponent<Controll>();
    }

    public void setStrength(float Rval, float Gval, float Bval) {
        Rstrength = Rval;
        Gstrength = Gval;
        Bstrength = Bval;

    }

    public void ResumePheros() {
        StartCoroutine(pheroDissipate(1f));
    }
    
    public void startPheros() {
        StartTime = TimeStepManager.Instance.totalFrameCount;
        lastTime = TimeStepManager.Instance.totalFrameCount;


        StartCoroutine(pheroDissipate(1f));
    }

    IEnumerator pheroDissipate(float delay) {
        //yield return new WaitForEndOfFrame();
        yield return new WaitForFixedUpdate();

        while(Rstrength > 0 || Gstrength > 0 || Bstrength > 0) {
            //yield return new WaitForSeconds(delay);
            for (int i = 0; i < (int)(delay * 10); i++)
                yield return new WaitForFixedUpdate();

            if (Rstrength > 0)
                Rstrength = Rstrength - (TimeStepManager.Instance.totalFrameCount - lastTime) / 2;
            if(Gstrength > 0) 
                Gstrength = Gstrength - (TimeStepManager.Instance.totalFrameCount - lastTime) / 2;
            if(Bstrength > 0) 
                Bstrength = Bstrength - (TimeStepManager.Instance.totalFrameCount - lastTime) / 2;

            lastTime = TimeStepManager.Instance.totalFrameCount;
        }

        //yield return new WaitForSeconds(5f);
        for (int i = 0; i < (int)(5 * 10); i++)
            yield return new WaitForFixedUpdate();

        Destroy(gameObject);
    }
}
