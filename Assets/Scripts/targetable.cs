﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class targetable : MonoBehaviour {

    private Controll controler;
    private float clickTime = 0;

    private void Start() {
        controler = GameObject.FindWithTag("Controller").GetComponent<Controll>();
    }

    void OnMouseOver() {
        if(Input.GetMouseButtonDown(0)) {
            clickTime = Time.unscaledTime;
        }

        if(Input.GetMouseButtonUp(0)) {
            float dt = Time.unscaledTime - clickTime;

            if(dt > 0f && dt < 0.15f) {
                controler.target(this.gameObject);
            }
            else {

            }

        }
    }
}
