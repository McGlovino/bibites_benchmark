﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.SettingScripts {

    
    public interface SimulationSetting {

        void ResetValue();

        void CreateUIElement(GameObject parent);

        void updateUIElement();

        void HideUIElement();

        void ShowUIElement();


    };

    [System.Serializable]
    public abstract class SimulationSetting<ValueType> : SimulationSetting {
        [System.NonSerialized]
        public string Name;
        [System.NonSerialized]
        public string HelperText;
        [System.NonSerialized]
        public string WikiLink;

        public ValueType DefaultValue;

        public ValueType Value;

        [System.NonSerialized]
        public List<SimulationSetting> DependantSettings;
        
        public abstract void CreateUIElement(GameObject parent);

        public abstract void updateUIElement();

        public abstract void HideUIElement();

        public abstract void ShowUIElement();

        public abstract void SetValue(ValueType value);

        public void ResetValue() {
            SetValue(DefaultValue);
        }

    }


    
}