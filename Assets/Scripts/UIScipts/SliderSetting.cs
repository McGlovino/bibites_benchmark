﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.SettingScripts;
using TMPro;

namespace Assets.Scripts.UIScipts {
    public class SliderSetting : MonoBehaviour{

        [SerializeField]
        private Slider slider;
        
        [SerializeField]
        private TMP_InputField editField;

        [SerializeField]
        private TextMeshProUGUI sliderValue;

        [SerializeField]
        private TextMeshProUGUI settingName;

        [SerializeField]
        private TextMeshProUGUI helperText;

        [SerializeField]
        private TextMeshProUGUI editText;

        [SerializeField]
        private SettingHelper HelperButton;

        [SerializeField]
        private GameObject defaultPointer; 

        FloatSetting fSetting;
        IntSetting iSetting;
            
        private int precision { get; set; }
        private string prefix { get; set; }
        private string suffix { get; set; }

        public void SetSettingInfo(FloatSetting _setting) {

            precision = _setting.precision;
            prefix = _setting.prefix;
            suffix = _setting.suffix;

            if(HelperButton != null) {
                HelperButton.WikiLink = _setting.WikiLink;
            }

            if(settingName != null) {
                settingName.text = _setting.Name;
            }
            
            if(editText != null) {
                editText.text = "Enter new value for "+_setting.Name+" :";
            }

            if(helperText != null) {
                helperText.text = "Info : \n" + _setting.HelperText;
                helperText.transform.parent.parent.gameObject.SetActive(false);
            }

            
            if(slider != null) {
                slider.minValue = _setting.minValue;
                slider.maxValue = _setting.maxValue;
                
                UpdateValue(_setting.Value);

                slider.onValueChanged.AddListener(ChangeFloatValue);
            }

            if(defaultPointer != null) {
                var sliderRT = slider.GetComponent<RectTransform>();
                var newPosRange = Vector2.zero + new Vector2(1f, 0f) * (sliderRT.rect.width);
                defaultPointer.transform.localPosition = newPosRange * (_setting.DefaultValue - _setting.minValue) / (_setting.maxValue - _setting.minValue);
            }

            if(editField != null) {
                editField.contentType = TMP_InputField.ContentType.DecimalNumber;
                editField.onEndEdit.AddListener(EditFloatValue);
            }

            fSetting = _setting;
        }

        public void SetSettingInfo(IntSetting _setting) {

            precision = 0;
            prefix = _setting.prefix;
            suffix = _setting.suffix;

            if(HelperButton != null) {
                HelperButton.WikiLink = _setting.WikiLink;
            }

            if(settingName != null) {
                settingName.text = _setting.Name;
            }

            if(helperText != null) {
                helperText.text = "Info : \n" + _setting.HelperText;
                helperText.transform.parent.parent.gameObject.SetActive(false);
            }

            if(slider != null) {
                slider.minValue = _setting.minValue;
                slider.maxValue = _setting.maxValue;
                slider.wholeNumbers = true;
                
                UpdateValue(_setting.Value);

                slider.onValueChanged.AddListener(ChangeIntValue);
            }

            if(editField != null) {
                editField.contentType = TMP_InputField.ContentType.IntegerNumber;
                editField.onEndEdit.AddListener(EditIntValue);
            }

            iSetting = _setting;
        }

        public void UpdateValue(float value) {
            slider.value = value;
            sliderValue.text = prefix + value.ToString("F" + precision) + suffix;
        }

        public void ChangeFloatValue(float value) {
            fSetting.SetValue(value); 
            sliderValue.text = prefix + value.ToString("F" + precision) + suffix;
        }

        public void ChangeIntValue(float value) {
            iSetting.SetValue((int)value); 
            sliderValue.text = prefix + value.ToString("F" + precision) + suffix;
        }

        public void EditFloatValue(string value) {
            float val = 0f;
            try {
                val = float.Parse(value.Replace(".",","));
                UpdateValue(val);
                fSetting.SetValue(val);
            }
            catch {}
        }

        public void EditIntValue(string value) {
            int val = int.Parse(value);
            UpdateValue(val);
            iSetting.SetValue(val);
        }
    
        public void ResetValue() {
            if(fSetting != null) {
                UpdateValue(fSetting.DefaultValue);
                fSetting.ResetValue();
            }

            if(iSetting != null) {
                UpdateValue(iSetting.DefaultValue);
                iSetting.ResetValue();
            }
        }

        private void OnRectTransformDimensionsChange() {
            if(fSetting != null) {
                if(defaultPointer != null) {
                    var sliderwidth = defaultPointer.transform.parent.GetComponent<RectTransform>().rect.width;
                    var newPosRange = Vector2.zero + new Vector2(1f, 0f) * sliderwidth;

                    defaultPointer.GetComponent<RectTransform>().localPosition = newPosRange * (fSetting.DefaultValue - fSetting.minValue) / (fSetting.maxValue - fSetting.minValue) - new Vector2(sliderwidth/2f,0);
                }
            }

            if(iSetting != null) {
                if(defaultPointer != null) {
                    var sliderwidth = defaultPointer.transform.parent.GetComponent<RectTransform>().rect.width;
                    var newPosRange = Vector2.zero + new Vector2(1f, 0f) * sliderwidth;

                    defaultPointer.GetComponent<RectTransform>().localPosition = newPosRange * (iSetting.DefaultValue - iSetting.minValue) / (iSetting.maxValue - iSetting.minValue) - new Vector2(sliderwidth / 2f, 0);
                }
            }
        }
    }
}
