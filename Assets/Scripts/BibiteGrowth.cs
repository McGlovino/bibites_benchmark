﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BibiteGrowth : MonoBehaviour
{   
    private geneCode gene;
    private NEATBrain brain;
    private player_control control;
    private Rigidbody2D rb2d;
    private BibiteBody body;

    [SerializeField]
    public float maturity;
    [SerializeField]
    private bool mature = false;

    private void InitGrowth() {
        rb2d = GetComponent<Rigidbody2D>();
        gene = GetComponent<geneCode>();
        brain = GetComponent<NEATBrain>();
        control = GetComponent<player_control>();
        body = GetComponent<BibiteBody>();
    }

    internal void ResumeGrowth() {
        InitGrowth();

        if(mature) {
            StartSystemsRequiringMraturity();
        }

        StartCoroutine(nameof(growing));
    }
    
    internal void StartGrowth(){
        InitGrowth();
        
        //measure 
        maturity = body.size/gene.genes[(int)geneCode.BibiteGenes.SizeRatio];
        mature = maturity >= 1f ? true : false;
        

        StartCoroutine(nameof(growing));
    }

    //This is the main growth loop
    IEnumerator growing() {
        setMaturity(maturity);

        //Growth event per seconds
        float freq = 5f;
        float growthdiff = 0f;

        //Growth cycles
        while(true) {
            for(int i = 0; i < (int)((1f / freq)*10); i ++)
                yield return new WaitForFixedUpdate();

            //How much to grow ?
            growthdiff = brain.outs[(int)NEATBrain.Outputs.Want2Grow] * growthDiff() / freq;
            //Size is increased
            setMaturity(maturity + growthdiff);
        }
    }


    //non-linear growth function (getting to maturity is easy and cheap, growing after that is harder and more costly)
    float growthDiff() {
        //return (1f + maturity - Mathf.Sqrt((maturity + 1f) * (maturity + 1f) - 1f)) / (5f * Mathf.Sqrt(gene.genes[(int)geneCode.BibiteGenes.SizeRatio]) / maturity);
        float diff = 0.1f / (1f + 3 * gene.genes[(int)geneCode.BibiteGenes.SizeRatio] * maturity * maturity);
        return diff;
    }

    //Changes impacted when bibite is growing.
    void setMaturity(float newMaturity){
        //growth is set
        maturity = newMaturity;
        
        //Size impacts are reported
        body.useEnergy(body.setSize(maturity * gene.genes[(int)geneCode.BibiteGenes.SizeRatio]));

        //If mature start related processes
        if(!mature && maturity >= 1f) {
            mature = true;
            StartSystemsRequiringMraturity();
        }

        //If not mature anymore stop processes
        if(mature && maturity < 1f) {
            mature = false;
        }
    }

    void StartSystemsRequiringMraturity() {
        StartCoroutine(control.layEggs());
    }
}
