﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectHandler
{
    public List<GameObject> objects;

    //Instance
    private static ObjectHandler OH;
    public static ObjectHandler Instance
    {
        get
        {
            if (OH == null)
                OH = new ObjectHandler();
            return OH;
        }
        set
        {
            OH = value;
        }
    }

    void Start()
    {
        ObjectHandler.Instance = this;

        if (objects == null)
            objects = new List<GameObject>();
    }

    public void AddObject(GameObject go)
    {
        if (objects == null)
            objects = new List<GameObject>();
        objects.Add(go);
    }

    public void RemoveObject(GameObject go)
    {
        objects.Remove(go);
    }
}
