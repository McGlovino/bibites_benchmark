﻿using UnityEngine;
using System.Collections;

public class srcGene : MonoBehaviour {
    public float layTime = 15f;
    public float broodTime = 10f;
    public float hatchTime = 5f;
    public float sizeRatio = 1f;
    public float speedRatio = 1f;
    public float colorR = 0f;
    public float colorG = 0f;
    public float colorB = 0f;
    public float strength = 5f;
    public float muteInt = 0.25f;
    public float muteChance = 0.25f;
    public float viewAngle = 50;
    public float viewRadius = 50;
    public float clkSpeed = 1f;
    public float diet = 0.5f;
    public float pherosense = 1f;

    // Update is called once per frame
    public void sourcify(geneCode gene) {
        gene.StartGenes();
        gene.genes[(int)geneCode.BibiteGenes.LayTime] = layTime;
        gene.genes[(int)geneCode.BibiteGenes.BroodTime] = broodTime;
        gene.genes[(int)geneCode.BibiteGenes.HatchTime] = hatchTime;
        gene.genes[(int)geneCode.BibiteGenes.SizeRatio] = sizeRatio;
        gene.genes[(int)geneCode.BibiteGenes.SpeedRatio] = speedRatio;
        gene.genes[(int)geneCode.BibiteGenes.ColorR] = colorR;
        gene.genes[(int)geneCode.BibiteGenes.ColorG] = colorG;
        gene.genes[(int)geneCode.BibiteGenes.ColorB] = colorB;
        gene.genes[(int)geneCode.BibiteGenes.Strength] = strength;
        gene.genes[(int)geneCode.BibiteGenes.MutationAmountSigma] = muteInt;
        gene.genes[(int)geneCode.BibiteGenes.AverageMutationNumber] = muteChance;
        gene.genes[(int)geneCode.BibiteGenes.ViewAngle] = viewAngle;
        gene.genes[(int)geneCode.BibiteGenes.ViewRadius] = viewRadius;
        gene.genes[(int)geneCode.BibiteGenes.ClockSpeed] = clkSpeed;
        gene.genes[(int)geneCode.BibiteGenes.PheroSense] = pherosense;
        gene.genes[(int)geneCode.BibiteGenes.Diet] = diet;
    }
}
