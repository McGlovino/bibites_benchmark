﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.UIScipts {
    public class UIReferenceHolder:MonoBehaviour {

        public static UIReferenceHolder Instance;


        [Header("UI Prefabs")]
        public GameObject FloatSettingPrefab;
        public GameObject IntSettingPrefab;
        public GameObject BoolSettingPrefab;

        [Header("SettingsHolders")]
        public GameObject SimulationOptionsHolder;
        public GameObject WorldParametersHolder;
        public GameObject PhysicsParametersHolder;
        public GameObject BibiteEnergyHolder;
        public GameObject MeatBalanceHolder;

        public GameObject VisualGenesHolder;
        public GameObject MutationGenesHolder;
        public GameObject DefaultGenesHolder;

        private void Awake() {
            Instance = this;
        }
    }
}
