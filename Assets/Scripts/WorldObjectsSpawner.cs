﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldObjectsSpawner : MonoBehaviour, ISerializationCallbackReceiver {
    [Header("World prefabs")]
    public GameObject bibiteEntry;
    
    public GameObject pelletEntry;
    
    public GameObject meatEntry;
    
    public GameObject eggEntry;

    public GameObject pelletSpawner;

    public GameObject bibiteSpawner;

    public GameObject pheromonesSource;

    [Header("Object References")]
    [SerializeField]
    public GameObject pelletHolder;
    public static GameObject PelletHolder;

    [SerializeField]
    public GameObject bibiteHolder;
    public static GameObject BibiteHolder;

    [SerializeField]
    public GameObject pheromoneHolder;
    public static GameObject PheromoneHolder;

    public Material sickMaterial;
    public static Material SickMaterial;

    public void OnAfterDeserialize() {
        PelletHolder = pelletHolder;
        BibiteHolder = bibiteHolder;
        PheromoneHolder = pheromoneHolder;
        SickMaterial = sickMaterial;
    }

    public void OnBeforeSerialize(){}

    public GameObject generateNewBibite() {
        return Instantiate(bibiteEntry, Vector3.zero, Quaternion.identity, BibiteHolder.transform);
    }
    
    public GameObject generateNewEgg() {
        return Instantiate(eggEntry, Vector3.zero, Quaternion.identity, BibiteHolder.transform);
    }
    
    public GameObject generateNewPellet() {
        return Instantiate(pelletEntry, Vector3.zero, Quaternion.identity, PelletHolder.transform);
    }
    public GameObject generateNewMeat() {
        return Instantiate(meatEntry, Vector3.zero, Quaternion.identity, PelletHolder.transform);
    }
    
    public GameObject generateNewPelletSpawner() {
        return Instantiate(pelletSpawner, Vector3.zero, Quaternion.identity);
    }
    
    public GameObject generateNewBibiteSpawner() {
        return Instantiate(bibiteSpawner, Vector3.zero, Quaternion.identity);
    }
    
    public GameObject generateNewPheromonesSource() {
        return Instantiate(pheromonesSource, Vector3.zero, Quaternion.identity, PheromoneHolder.transform);
    }

    


}
