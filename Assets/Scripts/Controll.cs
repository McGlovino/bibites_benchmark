﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.SettingScripts;

public class Controll : MonoBehaviour {
    private Canvas canv;

    public guiManager guimanager;
    public GameObject targeted;
    private GameObject[] pheroses;

    public GameObject autoPanner;

    public bool follow;

    // Use this for initialization
    void Start() {
        targeted = GameObject.Find("interfaceManager");
        guimanager = targeted.GetComponent<guiManager>();

        canv = GameObject.Find("UICanvas").GetComponent<Canvas>();
        targeted = null;
    }

    // Update is called once per frame
    void Update() {
        if(targeted != null & follow) {
            Camera.main.transform.position = targeted.transform.position + new Vector3(0, 0, -855);
        }

        if(Input.GetKeyDown(KeyCode.H)) {
            Cursor.visible = !Cursor.visible;
            canv.gameObject.SetActive(!canv.gameObject.activeSelf);
        }

        if(Input.GetKeyDown(KeyCode.K)) {
            targeted = autoPanner;
            follow = true;
        }

        // Random Bibite
        if(Input.GetKeyDown(KeyCode.R)) {
            GameObject[] targets = GameObject.FindGameObjectsWithTag("bibite");
            if (targets.Length > 0)
                selectBibiteOrEgg(targets[Random.Range(0, targets.Length)]);
        }

        // Highest Generation Bibite
        if(Input.GetKeyDown(KeyCode.G)) {
            GameObject[] targets = GameObject.FindGameObjectsWithTag("bibite");
            float oldestGen = 0;
            GameObject target = null;
            for(int i = 0; i < targets.Length; i++) {
                float curGen = targets[i].GetComponent<geneCode>().generation;
                if(curGen > oldestGen) {
                    oldestGen = curGen;
                    target = targets[i];
                }
            }

            if (target != null)
                selectBibiteOrEgg(target);
        }

        // Oldest Bibite
        if(Input.GetKeyDown(KeyCode.O)) {
            GameObject[] targets = GameObject.FindGameObjectsWithTag("bibite");
            float oldestTimeAlive = 0;
            GameObject target = null;
            for(int i = 0; i < targets.Length; i++) {
                player_control pc = targets[i].GetComponent<player_control>();
                if(pc == null || pc.clk == null) { continue; }

                float curTimeAlive = pc.clk.timeAlive;
                if(curTimeAlive > oldestTimeAlive) {
                    oldestTimeAlive = curTimeAlive;
                    target = targets[i];
                }
            }

            if (target != null)
                selectBibiteOrEgg(target);
        }

        // Random Egg
        if(Input.GetKeyDown(KeyCode.E)) {
            GameObject[] targets = GameObject.FindGameObjectsWithTag("egg");
            if (targets.Length > 0)
                selectBibiteOrEgg(targets[Random.Range(0, targets.Length)]);
        }
    }

    private void selectBibiteOrEgg(GameObject t) {
        if(t != null) {
            targeted = t;
            guimanager.targetThat(t);
            follow = true;
        }
    }

    public void noCible() {
        if(targeted != null) { 
            targeted = null;
            guimanager.clearTarget();
        }
    }

    public void target(GameObject toCible) {
        targeted = toCible;
        guimanager.targetThat(toCible);
        follow = true;
    }

    public bool compareTarget(GameObject GO) {
        return GO == targeted;
    }

    public void LayEggFromTargetBibite() {
        if(targeted != null) {
            var cont = targeted.GetComponent<player_control>();

            if(cont != null) {
                cont.LayEgg();
            }
        }
    }

    public void KillTargetBibite() {
        if(targeted != null) {
            var body = targeted.GetComponent<BibiteBody>();
            var egg = targeted.GetComponent<eggHatch>();
            
            if(body != null) {
                body.die();
            }else if(egg != null) {
                egg.abort();
            }
        }
    }
}