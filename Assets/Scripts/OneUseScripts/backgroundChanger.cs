﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.SettingScripts;


namespace Assets.Scripts.OneUseScripts {
    public class backgroundChanger : MonoBehaviour
    {
        public Slider slider;
        public Image sample;

        // Start is called before the first frame update
        void Start()
        {
            slider = GetComponent<Slider>();
            slider.value = GameSettings.Instance.backgroundShade.Value;
            sample.color = Color.white * GameSettings.Instance.backgroundShade.Value;
        }

        public void UpdateBackgroundShade(float val) {
            GameSettings.Instance.backgroundShade.SetValue(val);
            sample.color = Color.white * val;
            Camera.main.backgroundColor = Color.white * val;
        }

    }

}
