﻿using System;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

using Assets.Scripts.SettingScripts;
using RandomExtansion;

public class geneCode : MonoBehaviour {
    
    public float[] genes;

    private float willMute;
    private int muteChoice;
    
    public int generation;
    internal bool isReady = false;
    internal bool geneTransfered = false;
    
    public bool deuxParent;
    
    public int nGene;

    public enum BibiteGenes {
        LayTime, BroodTime, HatchTime,
        SizeRatio, SpeedRatio,
        ColorR, ColorG, ColorB,
        Strength,
        MutationAmountSigma, AverageMutationNumber,
        ViewAngle, ViewRadius,
        ClockSpeed,
        PheroSense,
        Diet, //(0 = herbivorous, .5 omnivore, 1 = carnivorous,)
    }

    // Use this for initialization
    public void StartGenes() {
        nGene = System.Enum.GetValues(typeof(BibiteGenes)).Length;
        genes = new float[nGene];
    }

    public void copyGene(geneCode genetoCopy, bool increaseGeneration = false) {
        StartGenes();
        for(int i = 0; i < nGene; i++) {
            genes[i] = genetoCopy.genes[i];
        }
        capGenes();

        if(increaseGeneration) { 
            generation = genetoCopy.generation + 1;
        }

        deuxParent = genetoCopy.deuxParent;
        geneTransfered = true;
    }

    public void copyGene2(geneCode gene, geneCode gene2) {

        bool c = true;
        int cmt = 0;
        int cmp = randChi(nGene);
        for(int i = 0; i < nGene; i++) {
            if(c)
                genes[i] = gene.genes[i];
            else
                genes[i] = gene2.genes[i];


            cmt++;
            if(cmt >= cmp) {
                c = !c;
                cmp = randChi(nGene - cmt);
                cmt = 0;
            }

        }
        geneTransfered = true;
        deuxParent = true;
    }

    public void mutate(int? numberOfMutation = null) {

        var r = new System.Random(UnityEngine.Random.Range(int.MinValue, int.MaxValue));

        if(numberOfMutation == null) {

            int n = r.NextPoisson(genes[(int)BibiteGenes.AverageMutationNumber]);
            for(int i = 0; i < n; i++) {
                oneMutation(r);
            }

        }
        else {
            
            for(int i = 0; i < (int)numberOfMutation; i++) {
                oneMutation(r);
            }
        }

        

        
    }

    public void oneMutation(System.Random r) {
        int modgene = Random.Range(0, nGene);

        // If the mutation is for a color gene, and all the genes are currently at their starting values, then choose a random value in the whole range
        // instead of a slowly migrating one. 
        if(
            (modgene == (int)BibiteGenes.ColorR ||
                modgene == (int)BibiteGenes.ColorG ||
                modgene == (int)BibiteGenes.ColorB) &&
                (genes[(int)BibiteGenes.ColorR] == 0f &&
                genes[(int)BibiteGenes.ColorG] == 0f &&
                genes[(int)BibiteGenes.ColorB] == 0f)
           ) {
            genes[(int)BibiteGenes.ColorR] = Random.Range(.01f, .15f);
            genes[(int)BibiteGenes.ColorG] = Random.Range(.01f, .15f);
            genes[(int)BibiteGenes.ColorB] = Random.Range(.01f, .15f);
        }
        else {

            //Mix a main relative mutation, with a small amount of absolute mutation to prevent being stuck at 0f;
            float relativeMutation = r.NextRelativeGaussianMutation(genes[modgene], genes[(int)BibiteGenes.MutationAmountSigma]);
            float absoluteMutation = r.NextGaussian(0f, 0.01f + genes[(int)BibiteGenes.MutationAmountSigma] / 20f);

            genes[modgene] += relativeMutation + absoluteMutation;

            
        }

        capGenes();
    }

    public void capGenes() {

        genes[0] = Mathf.Min(Mathf.Max(GameSettings.Instance.layTime.minValue, genes[0]), GameSettings.Instance.layTime.maxValue);
        genes[1] = Mathf.Min(Mathf.Max(GameSettings.Instance.broodTime.minValue, genes[1]), GameSettings.Instance.broodTime.maxValue);
        genes[2] = Mathf.Min(Mathf.Max(GameSettings.Instance.hatchTime.minValue, genes[2]), GameSettings.Instance.hatchTime.maxValue);
        genes[3] = Mathf.Min(Mathf.Max(GameSettings.Instance.sizeRatio.minValue, genes[3]), GameSettings.Instance.sizeRatio.maxValue);
        genes[4] = Mathf.Min(Mathf.Max(GameSettings.Instance.speedRatio.minValue, genes[4]), GameSettings.Instance.speedRatio.maxValue);
        genes[5] = Mathf.Min(Mathf.Max(GameSettings.Instance.colorR.minValue, genes[5]), GameSettings.Instance.colorR.maxValue);
        genes[6] = Mathf.Min(Mathf.Max(GameSettings.Instance.colorG.minValue, genes[6]), GameSettings.Instance.colorG.maxValue);
        genes[7] = Mathf.Min(Mathf.Max(GameSettings.Instance.colorB.minValue, genes[7]), GameSettings.Instance.colorB.maxValue);
        genes[8] = Mathf.Min(Mathf.Max(GameSettings.Instance.strength.minValue, genes[8]), GameSettings.Instance.strength.maxValue);
        genes[9] = Mathf.Min(Mathf.Max(GameSettings.Instance.mutationVariance.minValue, genes[9]), GameSettings.Instance.mutationVariance.maxValue);
        genes[10] = Mathf.Min(Mathf.Max(GameSettings.Instance.mutationChance.minValue, genes[10]), GameSettings.Instance.mutationChance.maxValue);
        genes[11] = Mathf.Min(Mathf.Max(GameSettings.Instance.viewAngle.minValue, genes[11]), GameSettings.Instance.viewAngle.maxValue);
        genes[12] = Mathf.Min(Mathf.Max(GameSettings.Instance.viewRadius.minValue, genes[12]), GameSettings.Instance.viewRadius.maxValue);
        genes[13] = Mathf.Min(Mathf.Max(GameSettings.Instance.clkSpeed.minValue, genes[13]), GameSettings.Instance.clkSpeed.maxValue);
        genes[14] = Mathf.Min(Mathf.Max(GameSettings.Instance.pherosense.minValue, genes[14]), GameSettings.Instance.pherosense.maxValue);
        genes[15] = Mathf.Min(Mathf.Max(GameSettings.Instance.diet.minValue, genes[15]), GameSettings.Instance.diet.maxValue);


    }

    public int randChi(int nMax) {
        return (int)((4 * Mathf.Pow(Random.Range(0, 1f) - 0.5f, 3) + 0.5f) * nMax);

    }

    public Color getBibiteColor(float r = 0f, float g = 0f, float b = 0f) {

        float R = genes[(int)BibiteGenes.ColorR] + r;
        float G = genes[(int)BibiteGenes.ColorG] + g;
        float B = genes[(int)BibiteGenes.ColorB] + b;

        float m = Mathf.Min(R, G, B);

        float Rf = 1 + R - (G > B ? G : B);
        Rf = Rf > 1 ? 1 : Rf < 0 ? 0 : Rf;

        float Gf = 1 + G - (R > B ? R : B);
        Gf = Gf > 1 ? 1 : Gf < 0 ? 0 : Gf;

        float Bf = 1 + B - (R > G ? R : G);
        Bf = Bf > 1 ? 1 : Bf < 0 ? 0 : Bf;

        return new Color(Rf - m, Gf - m, Bf - m, 1);

    }

    public float getDefaultBornSize() {
        return genes[(int)BibiteGenes.SizeRatio] * Mathf.Pow(genes[(int)BibiteGenes.HatchTime] / genes[(int)BibiteGenes.BroodTime],2f);
    }

    public float getBodyEnergyRatio() {

        return GameSettings.Instance.baseBodyEnergyRatio.Value
            + genes[(int)BibiteGenes.ViewAngle] / GameSettings.Instance.viewAngleBodyCost.Value
            + genes[(int)BibiteGenes.ViewRadius] / GameSettings.Instance.viewRadiusBodyCost.Value
            + genes[(int)BibiteGenes.Strength] / GameSettings.Instance.strengthBodyCost.Value;
    }
    
    public float getLayEnergy() {
        return 100f * getDefaultBornSize() * (
            GameSettings.Instance.maxEnergyToMaxHealthRatio.Value
            + getBodyEnergyRatio()*(
                1f + 1f / GameSettings.Instance.healthBodyRatio.Value));

    }

    public float getBirthBodyEnergy() {
        return 100f * getDefaultBornSize() * getBodyEnergyRatio() * (1f + 1f / GameSettings.Instance.healthBodyRatio.Value);
    }
}
