﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.SettingScripts {
    
    public class SettingGroup {
        public List<SimulationSetting> Settings;

        public void CreateUIElements(GameObject parent) {
            if(Settings != null) {
                foreach(SimulationSetting settting in Settings) {

                    settting.CreateUIElement(parent);

                }
            }
        }

        public void ResetValuesToDefault() {
            if(Settings != null) {
                foreach(SimulationSetting settting in Settings) {

                    settting.ResetValue();

                }
            }
        }

        public void UpdateUIElements() {
            if(Settings != null) {

                foreach(SimulationSetting settting in Settings) {

                    settting.updateUIElement();

                }
            }
        }


    }
}
