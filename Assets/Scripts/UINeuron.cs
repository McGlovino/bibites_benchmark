﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UINeuron : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {
    [SerializeField]
    private float value = 0;
    private float prevValue = 0;
    public Text overText;
    private Text genText;
    private string desc = "temp";
    public int index;

    private RawImage img;

    public List<int> siblingSynapses;
    public List<int> siblingNodes;
    public NEATBrain brain;
    public guiManager gM;

    public static float thickness = 1f;
    public static float highlightedThickness = 1.1f;

    private void Start() {
        overText.gameObject.SetActive(false);
        img = GetComponent<RawImage>();
        genText = GameObject.Find("BrainText").GetComponent<Text>();

    }
    private void Update() {
        if(prevValue != value) {
            prevValue = value;
            overText.text = "" + Mathf.Round(100f * value) / 100f;

            img.color = getColorGradiant(value);
        }

    }

    public void OnPointerEnter(PointerEventData eventData) {
        overText.gameObject.SetActive(true);
        genText.text = desc;

        if(siblingNodes.Count <= 0 || siblingSynapses.Count <= 0) {
            siblingNodes = new List<int>();
            siblingSynapses = new List<int>();
            if(brain.nodes[index].synapsIn != null && brain.nodes[index].synapsIn.Count > 0) {
                siblingSynapses = brain.GetConnectedSynapses(brain.nodes[index].synapsIn[0], new List<int>());
                siblingNodes = brain.GetConnectedNeurons(brain.nodes[index].synapsIn[0], new List<int>());
            }
            else if(brain.nodes[index].synapsOut != null && brain.nodes[index].synapsOut.Count > 0) {
                siblingSynapses = brain.GetConnectedSynapses(brain.nodes[index].synapsOut[0], new List<int>());
                siblingNodes = brain.GetConnectedNeurons(brain.nodes[index].synapsOut[0], new List<int>());
            }
        }

        gM.hightlightSubNetwork(siblingSynapses, siblingNodes);
        transform.SetAsLastSibling();
        gameObject.GetComponent<RectTransform>().sizeDelta 
            = new Vector2(highlightedThickness + 0.15f, highlightedThickness + 0.15f);
    }

    public void OnPointerExit(PointerEventData eventData) {
        overText.gameObject.SetActive(false);
        genText.text = "";

        gM.unHightlightSubNetwork(siblingSynapses, siblingNodes);
        transform.SetAsLastSibling();
        gameObject.GetComponent<RectTransform>().sizeDelta 
            = new Vector2(thickness, thickness);
    }

    public void setValue(float _value) {
        value = _value;
        overText.text = "" + Mathf.Round(100f * value) / 100f;
    }

    public void setDesc(string _desc) {
        desc = _desc;
    }

    private Color getColorGradiant(float val) {
        float gradia = (Mathf.Exp(2.5f * val) - 1) / (1 + Mathf.Exp(2.5f * val));

        if(val >= 0)
            return new Color((1 - gradia), 1f, (1 - gradia), 1f);
        else
            return new Color(1f, (gradia + 1), (gradia + 1), 1f);

    }
}
